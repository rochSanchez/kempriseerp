﻿using ERP.Infrastructure.Controllers;
using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Events.Security;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Regions;
using ERP.Sales.QuickAccess;
using ERP.Sales.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;

namespace ERP.Sales.Controller
{
    public class SalesController : ControllerBase
    {
        public SalesController(IEventAggregator eventAggregator, IRegionManager regionManager,
            IUnityContainer container) : base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<LoginSuccessfulEvent>().Subscribe(OnUserLoginSuccessful, ThreadOption.UIThread);
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenManagementModule);
        }

        private void OnOpenManagementModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Sales)
                obj.QuickAccess.IsSelected = true;
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<SalesController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(SalesWorkAreaView).FullName);
        }

        private void OnUserLoginSuccessful(LoginSuccessfulEventPayload obj)
        {
            RegionManager.RegisterViewWithRegion(RegionNames.QuickAccessRegion,
                () => Container.Resolve<SalesQuickAccessView>());
        }
    }
}