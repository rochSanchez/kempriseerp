﻿using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Sales.Controller;
using Prism.Commands;
using Prism.Events;
using Unity;

namespace ERP.Sales.QuickAccess
{
    public class SalesQuickAccessViewModel : NotifyPropertyChangedBase, IQuickAccess
    {
        private readonly IEventAggregator _eventAggregator;

        public SalesQuickAccessViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            _eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DeSelectModuleEvent<SalesController>>().Subscribe(OnDeSelectModule);

            OpenModuleCommand = new DelegateCommand(OpenModuleExecute);
        }

        public DelegateCommand OpenModuleCommand { get; set; }

        private void OpenModuleExecute()
        {
            _eventAggregator.GetEvent<OpenModuleEvent>().Publish(new OpenModuleEventPayload(ErpModules.Sales, this));
        }

        private void OnDeSelectModule()
        {
            IsSelected = false;
        }

        public bool IsSelected
        {
            get { return GetValue(() => IsSelected); }
            set { SetValue(() => IsSelected, value); }
        }
    }
}