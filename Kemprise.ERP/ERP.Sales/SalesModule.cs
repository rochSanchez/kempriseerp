﻿using ERP.Infrastructure.Controllers;
using ERP.Infrastructure.Modules;
using ERP.Sales.Controller;
using ERP.Sales.WorkArea;
using ERP.WebApi.DTO;
using Prism.Ioc;
using Prism.Unity;
using System.Reflection;
using Unity;
using Unity.Lifetime;

namespace ERP.Sales
{
    public class SalesModule : ErpModule
    {
        public SalesModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(SalesController),
                nameof(SalesController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(SalesModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer().RegisterType<object, SalesWorkAreaView>(typeof(SalesWorkAreaView).FullName,
                new ContainerControlledLifetimeManager());
        }
    }
}