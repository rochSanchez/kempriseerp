﻿using System.Windows.Controls;

namespace ERP.Sales.WorkArea
{
    /// <summary>
    /// Interaction logic for ManagementWorkAreaView.xaml
    /// </summary>
    public partial class SalesWorkAreaView : UserControl
    {
        public SalesWorkAreaView()
        {
            InitializeComponent();
        }
    }
}
