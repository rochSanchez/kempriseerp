﻿
namespace ERP.Client.Services.Interfaces
{
    public interface IMessageService
    {
        string GetMessage();
    }
}
