﻿using ERP.Client.Services.Interfaces;

namespace ERP.Client.Services
{
    public class MessageService : IMessageService
    {
        public string GetMessage()
        {
            return "Hello from the Message Service";
        }
    }
}
