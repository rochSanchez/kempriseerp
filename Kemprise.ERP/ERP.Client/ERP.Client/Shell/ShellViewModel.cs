﻿using ERP.Infrastructure.Events.Errors;
using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.Regions;
using ERP.Management.Controller;
using ERP.Sales.Controller;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;

namespace ERP.Client.Shell
{
    public class ShellViewModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IRegionManager _regionManager;

        public ShellViewModel()
        {
            _eventAggregator = ContainerLocator.Current.Resolve<IEventAggregator>();
            _regionManager = ContainerLocator.Current.Resolve<IRegionManager>();

            _eventAggregator.GetEvent<ErrorEvent>().Subscribe(OnExceptionCaught);
            GoHomeCommand = new DelegateCommand<object>(GoHomeExecute);
        }

        public DelegateCommand<object> GoHomeCommand { get; set; }

        private void GoHomeExecute(object obj)
        {
            var mainRegion = _regionManager.Regions[RegionNames.MainWorkAreaRegion];
            if (mainRegion.ActiveViews.Any())
                _regionManager.Regions[RegionNames.MainWorkAreaRegion].ActiveViews.ForEach(mainRegion.Remove);

            _eventAggregator.GetEvent<DeSelectModuleEvent<ManagementController>>().Publish();
            _eventAggregator.GetEvent<DeSelectModuleEvent<SalesController>>().Publish();
        }

        private void OnExceptionCaught(ErrorEventPayload obj)
        {
            var parameters = new DialogParameters
            {
                Content = obj.Message,
                DialogStartupLocation = WindowStartupLocation.CenterOwner,
                Owner = Application.Current.MainWindow
            };

            RadWindow.Alert(parameters);
        }
    }
}