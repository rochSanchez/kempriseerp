﻿using Telerik.Windows.Controls.Navigation;

namespace ERP.Client.Shell
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView
    {
        public ShellView()
        {
            InitializeComponent();
            RadWindowInteropHelper.SetAllowTransparency(this, false);
        }
    }
}