﻿using Prism.Ioc;
using Prism.Modularity;

namespace ERP.Security
{
    public class SecurityModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}