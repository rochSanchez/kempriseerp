﻿using System.Windows;

namespace ERP.Security.Login
{
    /// <summary>
    ///     Interaction logic for UserLoginView.xaml
    /// </summary>
    public partial class UserLoginView
    {
        public UserLoginView()
        {
            InitializeComponent();
        }

        private void RadPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext is UserLoginViewModel dataContext)
                dataContext.Password = PasswordBox.Password;
        }
    }
}