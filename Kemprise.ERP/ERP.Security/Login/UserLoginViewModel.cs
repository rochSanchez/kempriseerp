﻿using System;
using ERP.Infrastructure.Events.Errors;
using ERP.Infrastructure.Events.Security;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using Prism.Commands;
using Prism.Events;

namespace ERP.Security.Login
{
    public class UserLoginViewModel : NotifyPropertyChangedBase
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly IApiRequestManager _requestManager;

        public UserLoginViewModel(IEventAggregator eventAggregator, IApiRequestManager requestManager)
        {
            _eventAggregator = eventAggregator;
            _requestManager = requestManager;
            UserLoginCommand = new DelegateCommand(LoginExecute, LoginCanExecute);
            UserName = "Roch.Sanchez";
            Password = "RochSanchez_24";
        }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(() => IsBusy, value); }
        }

        public string LoginErrorMessage
        {
            get { return GetValue(() => LoginErrorMessage); }
            set { SetValue(() => LoginErrorMessage, value); }
        }

        public string Password
        {
            get { return GetValue(() => Password); }
            set
            {
                SetValue(() => Password, value, () =>
                {
                    LoginErrorMessage = "";
                    UserLoginCommand.RaiseCanExecuteChanged();
                });
            }
        }

        public DelegateCommand UserLoginCommand { get; }

        public string UserName
        {
            get { return GetValue(() => UserName); }
            set
            {
                SetValue(() => UserName, value, () =>
                {
                    LoginErrorMessage = "";
                    UserLoginCommand.RaiseCanExecuteChanged();
                });
            }
        }

        private bool LoginCanExecute()
        {
            return !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);
        }

        private async void LoginExecute()
        {
            try
            {
                IsBusy = true;

                var result = await _requestManager.LoginAsync(UserName, Password);

                IsBusy = false;
                if (result.IsSuccessful)
                    _eventAggregator.GetEvent<LoginSuccessfulEvent>()
                        .Publish(new LoginSuccessfulEventPayload
                        {
                            IsSuccessful = true
                        });
                else
                    _eventAggregator.GetEvent<ErrorEvent>()
                        .Publish(result.ErrorMessage == ""
                            ? new ErrorEventPayload(
                                "Login failed. Please try again.")
                            : new ErrorEventPayload($"Status Code {result.StatusCode} : {result.ErrorMessage}."));
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}