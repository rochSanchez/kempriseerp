﻿using ERP.Infrastructure.Controllers;
using ERP.Infrastructure.Events.Security;
using ERP.Infrastructure.Regions;
using ERP.Security.Login;
using Prism.Events;
using Prism.Regions;
using Unity;

namespace ERP.Security.Controller
{
    public class SecurityController : ControllerBase
    {
        private readonly UserLoginView _userLoginView;

        public SecurityController(IEventAggregator eventAggregator, IRegionManager regionManager,
            IUnityContainer container) : base(eventAggregator, regionManager, container)
        {
            EventAggregator.GetEvent<RequestUserCredentialsEvent>().Subscribe(OnUserLogin);
            EventAggregator.GetEvent<LoginSuccessfulEvent>().Subscribe(OnUserLoginSuccessful);

            _userLoginView = Container.Resolve<UserLoginView>();
        }

        private void OnUserLoginSuccessful(LoginSuccessfulEventPayload obj)
        {
            if (obj.IsSuccessful)
                RegionManager.Regions[RegionNames.DialogRegion].Deactivate(_userLoginView);
        }

        private void OnUserLogin()
        {
            RegionManager.RegisterViewWithRegion(RegionNames.DialogRegion, () => _userLoginView);
        }
    }
}