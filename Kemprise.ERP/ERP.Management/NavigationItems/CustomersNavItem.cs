﻿using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Modules.Navigation;
using ERP.Infrastructure.Services.APICommon;
using ERP.Management.ManagementViews.Customer;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace ERP.Management.NavigationItems
{
    public class CustomersNavItem : ParentNavigationItemBase
    {
        private readonly CustomerWorkAreaView _workAreaView;

        public CustomersNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<CustomerWorkAreaView>();
        }

        public override string Name => "Customers";

        public override Uri Icon => new Uri("/ERP.Client;component/Images/CustomerAlternate.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<CustomerListItem>(ApiControllers.Customer, "CustomerGetRecList");
        }
    }
}