﻿using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Modules.Navigation;
using ERP.Infrastructure.Services.APICommon;
using ERP.Management.ManagementViews.Vendors;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Management.Vendor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace ERP.Management.NavigationItems
{
    public class VendorNavItem : ParentNavigationItemBase
    {
        private readonly VendorWorkAreaView _workAreaView;

        public VendorNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<VendorWorkAreaView>();
        }

        public override string Name => "Vendors";

        public override Uri Icon => new Uri("/ERP.Client;component/Images/CustomerAlternate.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<VendorListItem>(ApiControllers.Vendor, "VendorGetRecList");
        }
    }
}