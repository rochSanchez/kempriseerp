﻿using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Modules.Navigation;
using ERP.Infrastructure.Services.APICommon;
using ERP.Management.ManagementViews.Employees;
using ERP.WebApi.DTO;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace ERP.Management.NavigationItems
{
    public class EmployeeNavItem : ParentNavigationItemBase
    {
        private readonly EmployeeWorkAreaView _workAreaView;

        public EmployeeNavItem(IUnityContainer container, IApiRequestManager requestManager) :
            base(container, requestManager)
        {
            _workAreaView = container.Resolve<EmployeeWorkAreaView>();
        }

        public override string Name => "Employees";

        public override Uri Icon => new Uri("/ERP.Client;component/Images/CustomerAlternate.png", UriKind.Relative);

        public override ErpModules Module => ErpModules.Management;

        public override object View => _workAreaView;

        public override async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await RequestManager.GetRecordsAsync<EmployeeListItem>(ApiControllers.Employee, "EmployeeGetRecList");
        }
    }
}