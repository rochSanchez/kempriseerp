﻿using ERP.Infrastructure.Events.Errors;
using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Modules.Navigation;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.WebApi.DTO;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace ERP.Management.WorkArea
{
    public class ManagementWorkAreaViewModel : NotifyPropertyChangedBase
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly Dictionary<string, IEnumerable<EntityListItem>> _itemsListDictionary;

        public ManagementWorkAreaViewModel(IUnityContainer container, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _itemsListDictionary = new Dictionary<string, IEnumerable<EntityListItem>>();

            NavigationItems = container.ResolveAll<IParentNavigationItem>()
                .Where(item => item.Module == ErpModules.Management)
                .OrderBy(item => item.Name);

            eventAggregator.GetEvent<RefreshItemListEvent>().Subscribe(OnRefreshItemList);
        }

        public string ViewName
        {
            get { return GetValue(() => ViewName); }
            set { SetValue(() => ViewName, value); }
        }

        public IEnumerable<INavigationItem> NavigationItems
        {
            get { return GetValue(() => NavigationItems); }
            set { SetValue(() => NavigationItems, value); }
        }

        public INavigationItem SelectedNavItem
        {
            get { return GetValue(() => SelectedNavItem); }
            set
            {
                SetValue(() => SelectedNavItem, value, () =>
                {
                    if (value?.View == null) return;
                    var viewModel = value.View.As<FrameworkElement>().GetViewModel();
                    RunStartup(viewModel, value.Name);
                });
            }
        }

        private async void RunStartup(object viewModel, string typeName)
        {
            var viewModelBase = viewModel.As<ManagementViewModelBase>();

            try
            {
                viewModelBase.IsBusy = true;
                IEnumerable<EntityListItem> entityListItems;

                if (_itemsListDictionary.ContainsKey(typeName))
                {
                    entityListItems = _itemsListDictionary[typeName].ToList();
                }
                else
                {
                    var result = await GetItemListAsync();
                    entityListItems = result as IList<EntityListItem> ?? result.ToList();
                    _itemsListDictionary.Add(typeName, entityListItems);
                }

                viewModelBase.EntityListItems = entityListItems;
                viewModelBase.CreateFilters();
            }
            catch (Exception e)
            {
                _eventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                viewModelBase.IsBusy = false;
            }
        }

        private async void OnRefreshItemList(RefreshItemListEventPayload obj)
        {
            var viewModelBase = obj.ViewModel.As<ManagementViewModelBase>();

            try
            {
                viewModelBase.IsBusy = true;
                var result = await GetItemListAsync();
                var entityListItems = result as IList<EntityListItem> ?? result.ToList();
                viewModelBase.EntityListItems = entityListItems;
                viewModelBase.CreateFilters();

                if (_itemsListDictionary.ContainsKey(obj.TypeName))
                    _itemsListDictionary[obj.TypeName] = entityListItems;
                else
                    _itemsListDictionary.Add(obj.TypeName, entityListItems);
            }
            catch (Exception e)
            {
                _eventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                viewModelBase.IsBusy = false;
            }
        }

        private Task<IEnumerable<EntityListItem>> GetItemListAsync()
        {
            return SelectedNavItem.GetRecordsAsync();
        }

        private void SetView(string viewName = "")
        {
            ViewName = viewName;
        }
    }
}