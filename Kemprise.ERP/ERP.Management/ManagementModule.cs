﻿using ERP.Infrastructure.Controllers;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Modules.Navigation;
using ERP.Management.Controller;
using ERP.Management.WorkArea;
using ERP.WebApi.DTO;
using Prism.Ioc;
using Prism.Unity;
using System.Linq;
using System.Reflection;
using Unity;
using Unity.Lifetime;
using Unity.RegistrationByConvention;

namespace ERP.Management
{
    public class ManagementModule : ErpModule
    {
        public ManagementModule(IUnityContainer container)
        {
            container.RegisterType(typeof(IControllerBase), typeof(ManagementController),
                nameof(ManagementController), new ContainerControlledLifetimeManager());
        }

        protected override Assembly GetBusinessAssembly()
        {
            return Assembly.GetAssembly(typeof(EntityListItem));
        }

        protected override Assembly GetModuleAssembly()
        {
            return Assembly.GetAssembly(typeof(ManagementModule));
        }

        public override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IParentNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.GetContainer()
                .RegisterTypes(
                    AllClasses.FromLoadedAssemblies().Where(x => typeof(IChildNavigationItem).IsAssignableFrom(x)),
                    WithMappings.FromAllInterfaces, WithName.TypeName, WithLifetime.ContainerControlled);

            containerRegistry.GetContainer()
                .RegisterType<object, ManagementWorkAreaView>(typeof(ManagementWorkAreaView).FullName,
                    new ContainerControlledLifetimeManager());
        }
    }
}