﻿using ERP.Infrastructure.Controllers;
using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Events.Security;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Regions;
using ERP.Management.QuickAccess;
using ERP.Management.WorkArea;
using Prism.Events;
using Prism.Regions;
using Unity;

namespace ERP.Management.Controller
{
    public class ManagementController : ControllerBase
    {
        public ManagementController(IEventAggregator eventAggregator, IRegionManager regionManager, IUnityContainer container) :
            base(eventAggregator, regionManager, container)
        {
            eventAggregator.GetEvent<LoginSuccessfulEvent>().Subscribe(OnUserLoginSuccessful, ThreadOption.UIThread);
            eventAggregator.GetEvent<OpenModuleEvent>().Subscribe(OnOpenManagementModule);
        }

        private void OnOpenManagementModule(OpenModuleEventPayload obj)
        {
            if (obj.ErpModule == ErpModules.Management)
                obj.QuickAccess.IsSelected = true;
            else
            {
                EventAggregator.GetEvent<DeSelectModuleEvent<ManagementController>>().Publish();
                return;
            }

            RegionManager.RequestNavigate(RegionNames.MainWorkAreaRegion, typeof(ManagementWorkAreaView).FullName);
        }

        private void OnUserLoginSuccessful(LoginSuccessfulEventPayload obj)
        {
            RegionManager.RegisterViewWithRegion(RegionNames.QuickAccessRegion, () => Container.Resolve<ManagementQuickAccessView>());
        }
    }
}