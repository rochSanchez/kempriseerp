﻿namespace ERP.Management.ManagementViews
{
    public enum ManagementTypes
    {
        Vendors,
        Customers,
        Employees
    }
}