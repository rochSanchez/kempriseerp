﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Files;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Employees.Methods
{
    public class EmployeeGetRec : GetMethodCallBase
    {
        public EmployeeGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "EmployeeGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Employees.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Employee;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var employeeAggregate = await GetRecordAsync<EmployeeAggregate>(MethodName, new object[] { MethodParameter });

            if (employeeAggregate.EmployeeDto != null)
                entity.SetProperty("EmployeeDto", new DynamicNotifyPropertyChangedProxy(employeeAggregate.EmployeeDto));

            if (employeeAggregate.EmployeeFiles.Any())
            {
                var employeeFiles = new ObservableCollection<FileDto>(employeeAggregate.EmployeeFiles);
                entity.SetProperty("EmployeeFiles", employeeFiles);
            }
        }
    }
}