﻿using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Employees.Methods
{
    public class EmployeeAddRec : ErpSaveMethodCallBase
    {
        public EmployeeAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "EmployeeAddRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Employee;
        public override string Category => ManagementTypes.Employees.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var employeeEntity = entity.As<EmployeeEntity>();

            var employeeAggregate = new EmployeeAggregate
            {
                EmployeeDto = employeeEntity.EmployeeDto.GetDto<EmployeeDto>(),
                EmployeeFiles = employeeEntity.EmployeeFiles.ToList()
            };

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(EmployeeAggregate), employeeAggregate }
            };

            await SaveNewRecordAsync(MethodName, dataToSave);
        }
    }
}