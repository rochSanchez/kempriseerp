﻿namespace ERP.Management.ManagementViews.Employees.EmployeeViews
{
    public class EmployeeViewNames
    {
        public const string EmployeeMainView = "Employee Main View";
        public const string EmployeeEditView = "Employee Edit View";
        public const string EmployeeDisplayView = "Employee Display View";
    }
}