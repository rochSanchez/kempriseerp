﻿using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.Files;
using ERP.Management.ManagementViews.Employees.EmployeeViews;
using ERP.WebApi.DTO.DTOs.Files;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace ERP.Management.ManagementViews.Employees
{
    public sealed class EmployeeWorkAreaViewModel : ManagementViewModelBase
    {
        private readonly IFileService _fileService;
        private EmployeeEntity _entity;

        public EmployeeWorkAreaViewModel(IUnityContainer container, IFileService fileService) :
            base(container, ManagementTypes.Employees.ToString())
        {
            _fileService = fileService;
            SetView(EmployeeViewNames.EmployeeMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            RefreshCommand = new DelegateCommand(RefreshExecute);

            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);

            UploadFileCommand = new DelegateCommand(UploadFileExecute);
            DownloadFileCommand = new DelegateCommand(DownloadFileExecute);

            Methods["EmployeeGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand UploadFileCommand { get; set; }
        public DelegateCommand DownloadFileCommand { get; set; }

        private void WhenGetRecDone()
        {
            _entity.EmployeeDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());
            _entity = Entity.GetDto<EmployeeEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(EmployeeViewNames.EmployeeDisplayView);
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());

            _entity = Entity.GetDto<EmployeeEntity>();
            _entity.IsNew = true;

            OpenItemExecute("");

            SetView(EmployeeViewNames.EmployeeEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent>()
                .Publish(new RefreshItemListEventPayload(this, "Employees"));
        }

        private void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select an Employee",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new EmployeeEntity());
            _entity = Entity.GetDto<EmployeeEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(EmployeeViewNames.EmployeeEditView);
        }

        private void BackExecute()
        {
            SetView(EmployeeViewNames.EmployeeMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var employeeDto = _entity.EmployeeDto.GetDto<EmployeeDto>();
            var properties = employeeDto.GetType().GetProperties();
            properties.ForEach(info =>
            {
                var propertyValue = info.GetValue(employeeDto, null);
                if (info.PropertyType == typeof(string) && propertyValue != null)
                    info.SetValue(employeeDto, propertyValue.ToString().ToUpper());
            });

            var taskToPerform = _entity.IsNew ? "EmployeeAddRec" : "EmployeeModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var employeeDto = _entity.EmployeeDto.GetDto<EmployeeDto>();
            return !string.IsNullOrEmpty(employeeDto.Name);
        }

        private void UploadFileExecute()
        {
            var files = _fileService.GetAnyFile();
            if (files == null) return;

            var fileList = files.ToList();
            var fileNames = fileList.Select(x => x.FileName).ToList();
            var duplicates = _entity.EmployeeFiles.Where(x => fileNames.Contains(x.FileName)).ToList();

            if (duplicates.Any())
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Duplicate File Names Found",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            fileList.ForEach(file => _entity.EmployeeFiles.Add(file));
        }

        private void DownloadFileExecute()
        {
            if (SelectedFile == null) return;
            _fileService.SaveFile(SelectedFile);
        }

        public override void CreateFilters()
        {
            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedItem = null;
        }

        public EmployeeListItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set { SetValue(() => SelectedItem, value); }
        }

        public FileDto SelectedFile
        {
            get { return GetValue(() => SelectedFile); }
            set { SetValue(() => SelectedFile, value); }
        }
    }
}