﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.WebApi.DTO.DTOs.Files;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using System.Collections.ObjectModel;

namespace ERP.Management.ManagementViews.Employees
{
    public class EmployeeEntity : EntityBase
    {
        public EmployeeEntity()
        {
            EmployeeDto = new DynamicNotifyPropertyChangedProxy(new EmployeeDto());

            EmployeeFiles = new ObservableCollection<FileDto>();
        }

        public DynamicNotifyPropertyChangedProxy EmployeeDto { get; set; }

        public ObservableCollection<FileDto> EmployeeFiles { get; set; }
        public FileDto FileToDelete { get; set; }
    }
}