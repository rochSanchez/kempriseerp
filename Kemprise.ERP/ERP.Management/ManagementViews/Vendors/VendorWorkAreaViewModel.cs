﻿using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Management.ManagementViews.Vendors.VendorViews;
using ERP.WebApi.DTO.DTOs.Management.Vendor;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace ERP.Management.ManagementViews.Vendors
{
    public sealed class VendorWorkAreaViewModel : ManagementViewModelBase
    {
        private VendorEntity _entity;

        public VendorWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Vendors.ToString())
        {
            SetView(VendorViewNames.VendorMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            RefreshCommand = new DelegateCommand(RefreshExecute);

            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);

            Methods["VendorGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }

        private void WhenGetRecDone()
        {
            _entity.VendorDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new VendorEntity());
            _entity = Entity.GetDto<VendorEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(VendorViewNames.VendorDisplayView);
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new VendorEntity());

            _entity = Entity.GetDto<VendorEntity>();
            _entity.IsNew = true;

            OpenItemExecute("");

            SetView(VendorViewNames.VendorEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent>()
                .Publish(new RefreshItemListEventPayload(this, "Vendors"));
        }

        private void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Vendor",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new VendorEntity());
            _entity = Entity.GetDto<VendorEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(VendorViewNames.VendorEditView);
        }

        private void BackExecute()
        {
            SetView(VendorViewNames.VendorMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var vendorDto = _entity.VendorDto.GetDto<VendorDto>();
            var properties = vendorDto.GetType().GetProperties();
            properties.ForEach(info =>
            {
                var propertyValue = info.GetValue(vendorDto, null);
                if (info.PropertyType == typeof(string) && propertyValue != null)
                    info.SetValue(vendorDto, propertyValue.ToString().ToUpper());
            });

            var taskToPerform = _entity.IsNew ? "VendorAddRec" : "VendorModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var vendorDto = _entity.VendorDto.GetDto<VendorDto>();
            return !string.IsNullOrEmpty(vendorDto.Name);
        }

        public override void CreateFilters()
        {
            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedItem = null;
        }

        public VendorListItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set { SetValue(() => SelectedItem, value); }
        }
    }
}