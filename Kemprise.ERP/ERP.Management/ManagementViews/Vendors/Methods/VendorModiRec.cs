﻿using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Management.Vendor;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Vendors.Methods
{
    public class VendorModiRec : ErpSaveMethodCallBase
    {
        public VendorModiRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "VendorModiRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Vendor;
        public override string Category => ManagementTypes.Vendors.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var vendorDto = entity.As<VendorEntity>().VendorDto.GetDto<VendorDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(VendorDto), vendorDto }
            };

            await SaveNewRecordAsync<VendorDto>(MethodName, dataToSave);
        }
    }
}