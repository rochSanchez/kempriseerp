﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Management.Vendor;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Vendors.Methods
{
    public class VendorGetRec : GetMethodCallBase
    {
        public VendorGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "VendorGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Vendors.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Vendor;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var vendorDto = await GetRecordAsync<VendorDto>(MethodName, new object[] { MethodParameter });
            entity.SetProperty("VendorDto", new DynamicNotifyPropertyChangedProxy(vendorDto));
        }
    }
}