﻿namespace ERP.Management.ManagementViews.Vendors.VendorViews
{
    public class VendorViewNames
    {
        public const string VendorMainView = "Vendor Main View";
        public const string VendorEditView = "Vendor Edit View";
        public const string VendorDisplayView = "Vendor Display View";
    }
}