﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.WebApi.DTO.DTOs.Management.Vendor;

namespace ERP.Management.ManagementViews.Vendors
{
    public class VendorEntity : EntityBase
    {
        public VendorEntity()
        {
            VendorDto = new DynamicNotifyPropertyChangedProxy(new VendorDto());
        }

        public DynamicNotifyPropertyChangedProxy VendorDto { get; set; }
    }
}