﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using System.Collections.Generic;

namespace ERP.Management.ManagementViews.Customer
{
    public class CustomerEntity : EntityBase
    {
        public CustomerEntity()
        {
            CustomerDto = new DynamicNotifyPropertyChangedProxy(new CustomerDto());
            TransactionHistory = new List<CustomerTransaction>();
        }

        public DynamicNotifyPropertyChangedProxy CustomerDto { get; set; }
        public List<CustomerTransaction> TransactionHistory { get; set; }
    }
}