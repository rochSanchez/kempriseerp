﻿namespace ERP.Management.ManagementViews.Customer.CustomerViews
{
    public class CustomerViewNames
    {
        public const string CustomerMainView = "Customer Main View";
        public const string CustomerEditView = "Customer Edit View";
        public const string CustomerDisplayView = "Customer Display View";
    }
}