﻿using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Customer.Methods
{
    public class CustomerAddRec : ErpSaveMethodCallBase
    {
        public CustomerAddRec(IApiRequestManager apiRequestManager) : base(apiRequestManager)
        {
        }

        public override string MethodName => "CustomerAddRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override ApiControllers Controller => ApiControllers.Customer;
        public override string Category => ManagementTypes.Customers.ToString();
        public override bool UseDefaultParameter => false;

        public override async Task SaveRecordAsync(EntityBase entity)
        {
            var customerDto = entity.As<CustomerEntity>().CustomerDto.GetDto<CustomerDto>();

            var dataToSave = new Dictionary<string, object>
            {
                { nameof(CustomerDto), customerDto }
            };

            await SaveNewRecordAsync<CustomerDto>(MethodName, dataToSave);
        }
    }
}