﻿using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using System.Threading.Tasks;

namespace ERP.Management.ManagementViews.Customer.Methods
{
    public class CustomerGetRec : GetMethodCallBase
    {
        public CustomerGetRec(IApiRequestManager requestManager) : base(requestManager)
        {
        }

        public override string MethodName => "CustomerGetRec";
        public override ErpModules ModuleName => ErpModules.Management;
        public override string Category => ManagementTypes.Customers.ToString();
        public override bool RunAtStartup => true;

        public override bool UseDefaultParameter => true;
        public override ApiControllers Controller => ApiControllers.Customer;

        public override async Task RunAsync(DynamicNotifyPropertyChangedProxy entity)
        {
            var customerDto = await GetRecordAsync<CustomerDto>(MethodName, new object[] { MethodParameter });
            entity.SetProperty("CustomerDto", new DynamicNotifyPropertyChangedProxy(customerDto));
        }
    }
}