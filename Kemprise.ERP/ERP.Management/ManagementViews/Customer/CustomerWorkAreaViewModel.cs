﻿using ERP.Infrastructure.Events.Modules;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Management.ManagementViews.Customer.CustomerViews;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace ERP.Management.ManagementViews.Customer
{
    public sealed class CustomerWorkAreaViewModel : ManagementViewModelBase
    {
        private CustomerEntity _entity;

        public CustomerWorkAreaViewModel(IUnityContainer container) :
            base(container, ManagementTypes.Customers.ToString())
        {
            SetView(CustomerViewNames.CustomerMainView);

            OpenCommand = new DelegateCommand(OpenExecute);
            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            RefreshCommand = new DelegateCommand(RefreshExecute);

            EditCommand = new DelegateCommand(EditExecute);
            BackCommand = new DelegateCommand(BackExecute);
            SaveCommand = new DelegateCommand(SaveExecute, CanSaveExecute);

            Methods["CustomerGetRec"].As<IErpGetMethodCall>().WhenDone += WhenGetRecDone;
        }

        public DelegateCommand EditCommand { get; set; }
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }

        private void WhenGetRecDone()
        {
            _entity.CustomerDto.PropertyChanged += (s, e) => { SaveCommand.RaiseCanExecuteChanged(); };
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void OpenExecute()
        {
            if (SelectedItem == null) return;

            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());
            _entity = Entity.GetDto<CustomerEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(CustomerViewNames.CustomerDisplayView);
        }

        private void AddNewRecordExecute()
        {
            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());

            _entity = Entity.GetDto<CustomerEntity>();
            _entity.IsNew = true;

            OpenItemExecute("");

            SetView(CustomerViewNames.CustomerEditView);
        }

        private void RefreshExecute()
        {
            SearchFor = "";
            EventAggregator.GetEvent<RefreshItemListEvent>()
                .Publish(new RefreshItemListEventPayload(this, "Customers"));
        }

        private void EditExecute()
        {
            if (SelectedItem == null)
            {
                RadWindow.Alert(new DialogParameters
                {
                    Content = "Please Select a Customer",
                    Owner = Application.Current.MainWindow,
                    DialogStartupLocation = WindowStartupLocation.CenterScreen
                });

                return;
            }

            Entity = new DynamicNotifyPropertyChangedProxy(new CustomerEntity());
            _entity = Entity.GetDto<CustomerEntity>();

            OpenItemExecute(SelectedItem.Id);

            SetView(CustomerViewNames.CustomerEditView);
        }

        private void BackExecute()
        {
            SetView(CustomerViewNames.CustomerMainView);
            RefreshExecute();
        }

        private async void SaveExecute()
        {
            var customerDto = _entity.CustomerDto.GetDto<CustomerDto>();
            var properties = customerDto.GetType().GetProperties();
            properties.ForEach(info =>
            {
                var propertyValue = info.GetValue(customerDto, null);
                if (info.PropertyType == typeof(string) && propertyValue != null)
                    info.SetValue(customerDto, propertyValue.ToString().ToUpper());
            });

            var taskToPerform = _entity.IsNew ? "CustomerAddRec" : "CustomerModiRec";
            var saveResult = await SaveRecordAsync(taskToPerform);
            if (saveResult.OperationResult != OperationResult.Success) return;

            BackExecute();
        }

        private bool CanSaveExecute()
        {
            var customerDto = _entity.CustomerDto.GetDto<CustomerDto>();
            return !string.IsNullOrEmpty(customerDto.Name);
        }

        public override void CreateFilters()
        {
            var view = new CollectionViewSource { Source = EntityListItems };
            CollectionView = view;

            SelectedItem = null;
        }

        public CustomerListItem SelectedItem
        {
            get { return GetValue(() => SelectedItem); }
            set { SetValue(() => SelectedItem, value); }
        }

        public CustomerTransaction SelectedTransactionHistory
        {
            get { return GetValue(() => SelectedTransactionHistory); }
            set { SetValue(() => SelectedTransactionHistory, value); }
        }
    }

    public class CustomerTransaction
    {
    }
}