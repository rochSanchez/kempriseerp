﻿using ERP.Infrastructure.Events.Errors;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.ViewModel;
using ERP.WebApi.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Unity;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace ERP.Management
{
    public class ManagementViewModelBase : ErpViewModelBase
    {
        protected ManagementViewModelBase(IUnityContainer container, string category) :
            base(container, ErpModules.Management, category)
        {
            // _reportLoaderService = container.Resolve<IReportLoaderService>();
            // _terminalReportDetails = ReportHelper.GetReports<ITerminalReportDetail>();

            DocumentPanes = new ObservableCollection<RadDocumentPane>();
            Filters = new ObservableCollection<Filter>();
            AddFilterCommand = new DelegateCommand(AddFilter, CanAddFilter);
            FilterCommand = new DelegateCommand(SetFilter);
            RemoveFilterCommand = new DelegateCommand(RemoveFilter, CanRemoveFilter);
            ClearAllCommand = new DelegateCommand(ClearAllExecute);
        }

        public string Id { get; set; }
        public ObservableCollection<RadDocumentPane> DocumentPanes { get; set; }
        public ObservableCollection<Filter> Filters { get; set; }
        public Dictionary<string, string> Fields { get; set; }
        public Dictionary<string, Action<string, string>> Criteria { get; set; }
        public Dictionary<string, string> FilterRegistrations { get; set; } = new Dictionary<string, string>();
        public DelegateCommand AddFilterCommand { get; set; }
        public DelegateCommand AddNewRecordCommand { get; set; }
        public DelegateCommand FilterCommand { get; set; }
        public DelegateCommand OpenCommand { get; set; }
        public DelegateCommand RefreshCommand { get; set; }
        public DelegateCommand ClearAllCommand { get; set; }
        public DelegateCommand RemoveFilterCommand { get; set; }

        public CollectionViewSource CollectionView
        {
            get { return GetValue(() => CollectionView); }
            set { SetValue(() => CollectionView, value); }
        }

        public IEnumerable<EntityListItem> EntityListItems
        {
            get { return GetValue(() => EntityListItems); }
            set { SetValue(() => EntityListItems, value); }
        }

        public string FilterValue
        {
            get { return GetValue(() => FilterValue); }
            set { SetValue(() => FilterValue, value, AddFilterCommand.RaiseCanExecuteChanged); }
        }

        public bool IsFilterValueReadOnly
        {
            get { return GetValue(() => IsFilterValueReadOnly); }
            set { SetValue(() => IsFilterValueReadOnly, value); }
        }

        public string SearchFor
        {
            get { return GetValue(() => SearchFor); }
            set
            {
                SetValue(() => SearchFor, value, () =>
                {
                    if (value == null) return;
                    var view = CollectionView == null
                        ? CollectionViewSource.GetDefaultView(EntityListItems)
                        : CollectionView.View;

                    view.Filter = obj =>
                    {
                        if (obj == null) return false;
                        return obj.ToString().IndexOf(value, StringComparison.OrdinalIgnoreCase) > -1;
                    };
                });
            }
        }

        public Action<string, string> SelectedCriteria
        {
            get { return GetValue(() => SelectedCriteria); }
            set { SetValue(() => SelectedCriteria, value, () => { AddFilterCommand.RaiseCanExecuteChanged(); }); }
        }

        public object SelectedCriteriaName
        {
            get { return GetValue(() => SelectedCriteriaName); }
            set
            {
                SetValue(() => SelectedCriteriaName, value, () =>
                {
                    var x = SelectedCriteriaName != null
                        ? SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "")
                        : "";
                    if (x == "Is empty" || x == "Is not empty")
                        IsFilterValueReadOnly = true;
                    else
                        IsFilterValueReadOnly = false;
                });
            }
        }

        public string SelectedField
        {
            get { return GetValue(() => SelectedField); }
            set { SetValue(() => SelectedField, value, AddFilterCommand.RaiseCanExecuteChanged); }
        }

        public Filter SelectedFilter
        {
            get { return GetValue(() => SelectedFilter); }
            set { SetValue(() => SelectedFilter, value, RemoveFilterCommand.RaiseCanExecuteChanged); }
        }

        public string ViewName
        {
            get { return GetValue(() => ViewName); }
            set { SetValue(() => ViewName, value); }
        }

        protected override void RegisterLookups()
        {
        }

        protected void DoesNotContainFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                var value = property?.GetValue(o, null);
                if (value == null) return false;
                return !value.ToString().Contains(s);
            };
        }

        protected void EndsWithFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                var value = property?.GetValue(o, null);
                return value != null && value.ToString().EndsWith(s);
            };
        }

        protected void EqualFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                var propertyValue = property?.GetValue(o, null)?.ToString();
                var result = propertyValue == s;
                return result;
            };
        }

        protected void GreaterThanFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                var result = int.Parse(s);
                var result2 = int.Parse(property.GetValue(o, null)?.ToString());
                return result2 > result;
            };
        }

        protected void IsEmptyFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var x = (T)o;
                var property = x.GetType().GetProperty(s1);
                var value = property?.GetValue(o, null);
                return value != null && string.IsNullOrEmpty(value.ToString());
            };
        }

        protected void IsNotEmptyFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                return !string.IsNullOrEmpty(property?.GetValue(o, null)?.ToString());
            };
        }

        protected void LessThanFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                var result = int.Parse(s);
                var result2 = int.Parse(property.GetValue(o, null)?.ToString());
                return result2 < result;
            };
        }

        protected void LikeFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var item = (T)o;
                var property = item.GetType().GetProperty(s1);
                return property != null && property.GetValue(o, null).ToString().Contains(s);
            };
        }

        protected async void OpenItemExecute(string parameter, object view, string header)
        {
            var viewModel = view.As<FrameworkElement>().GetViewModel().As<ManagementViewModelBase>();
            try
            {
                viewModel.IsBusy = true;
                viewModel.Id = parameter;
                CreatePane(view, header);
                var result = await viewModel.RunStartupMethodsAsync(parameter);
                var operationResponses = result as IList<OperationResponse> ?? result.ToList();
                if (operationResponses.Any(x => x.OperationResult == OperationResult.Failed))
                {
                    var error = operationResponses.FirstOrDefault();
                    if (error == null) return;
                    EventAggregator.GetEvent<ErrorEvent>()
                        .Publish(new ErrorEventPayload($"{error.ErrorMessage}"));
                }
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>().Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                viewModel.IsBusy = false;
            }
        }

        protected async void OpenItemExecute(string parameter)
        {
            try
            {
                IsBusy = true;
                Id = parameter;
                var result = await RunStartupMethodsAsync(parameter);
                var operationResponses = result as IList<OperationResponse> ?? result.ToList();
                if (operationResponses.Any(x => x.OperationResult == OperationResult.Failed))
                {
                    var error = operationResponses.FirstOrDefault();
                    if (error == null) return;
                    EventAggregator.GetEvent<ErrorEvent>()
                        .Publish(new ErrorEventPayload($"{error.ErrorMessage}"));
                }
            }
            catch (Exception e)
            {
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload($"{e.Message}"));
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected void RemovePane(object view)
        {
            var paneToRemove = DocumentPanes.FirstOrDefault(x => x.Content.As<ScrollViewer>().Content == view);
            if (paneToRemove != null)
                DocumentPanes.Remove(paneToRemove);
        }

        protected virtual void SetView(string viewName = "")
        {
            ViewName = viewName;
        }

        public virtual void CreateFilters()
        {
        }

        protected virtual void ClearAllExecute()
        {
        }

        protected void StartsWithFilter<T>(string s, string s1) where T : EntityListItem
        {
            CollectionView.View.Filter = o =>
            {
                if (o == null) return false;
                var x = (T)o;
                var property = x.GetType().GetProperty(s1);
                var value = property?.GetValue(o, null);
                return value != null && value.ToString().StartsWith(s);
            };
        }

        private void AddFilter()
        {
            var x = SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "");
            Filters.Add(new Filter
            {
                Action = SelectedCriteria,
                Text = SelectedField + " " + x + " " + FilterValue,
                Value = FilterValue,
                Field = SelectedField
            });
            Clear();
        }

        private bool CanAddFilter()
        {
            if (EntityListItems == null || !EntityListItems.Any()) return false;
            var x = SelectedCriteriaName != null
                ? SelectedCriteriaName.ToString().Split(',')[0].Trim().Replace("[", "")
                : "";
            bool result;
            if (x == "Is empty" || x == "Is not empty")
                result = SelectedCriteria == null || SelectedField == null;
            else
                result = string.IsNullOrEmpty(FilterValue) || SelectedCriteria == null || SelectedField == null;
            return !result;
        }

        private bool CanRemoveFilter()
        {
            return SelectedFilter != null;
        }

        private void Clear()
        {
            FilterValue = null;
            SelectedCriteria = null;
            SelectedField = null;
        }

        private void CreatePane(object view, string header)
        {
            var content = new ScrollViewer
            {
                Content = view,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto
            };

            var pane = new RadDocumentPane
            {
                Content = content,
                Header = header,
                Tag = Guid.NewGuid()
            };

            DocumentPanes.Add(pane);
        }

        private void RemoveFilter()
        {
            var item = Filters.First(x => x.Text == SelectedFilter.Text);
            Filters.Remove(item);
            CollectionView.View.Filter = o => true;
            ClearAllExecute();
        }

        private void SetFilter()
        {
            ClearAllExecute();
            if (SelectedFilter == null) return;
            var filterValue = SelectedFilter.Value;
            var fieldValue = SelectedFilter.Field;
            SelectedFilter.Action(filterValue, fieldValue);
        }

        public void RegisterAndFilter<T>(string propertyName, string value) where T : EntityListItem
        {
            if (FilterRegistrations.ContainsKey(propertyName))
                FilterRegistrations[propertyName] = value;
            else
                FilterRegistrations.Add(propertyName, value);

            Filter<T>();
        }

        private void Filter<T>() where T : EntityListItem
        {
            CollectionView.View.Filter = obj =>
            {
                if (obj == null) return false;
                var item = obj as T;
                var properties = item?.GetType().GetProperties();
                var filterCount = FilterRegistrations.Count(x => x.Value != null);
                var matches = Enumerable.ToList(
                    (from valuePair in FilterRegistrations.Where(filter => filter.Value != null)
                     let firstOrDefault = properties?.FirstOrDefault(x => x.Name == valuePair.Key)
                     let isDate = firstOrDefault?.GetValue(item) is DateTime
                     let value = isDate
                         ? ((DateTime)firstOrDefault?.GetValue(item)).ToShortDateString()
                         : firstOrDefault?.GetValue(item)?.ToString()
                     where value == valuePair.Value
                     select firstOrDefault));

                return filterCount == matches.Count;
            };
        }

        // protected void RunReport(string key)
        // {
        //     var terminalReport = _terminalReportDetails[key];
        //
        //     var data = terminalReport.ConvertObjectToDataTable(Entity.GetDto<EntityBase>(), Lookups);
        //     var data2 = terminalReport.ConvertObjectToDataTable2(Entity.GetDto<EntityBase>(), Lookups);
        //     var parameters = terminalReport.GetReportParameters(Entity.GetDto<EntityBase>(), Lookups);
        //
        //     var reportLoaderInfo = new TerminalReportLoaderInfo(data, parameters, terminalReport.ReportTitle,
        //         terminalReport.ReportFileName, terminalReport.TableName, data2, terminalReport.TableName2);
        //
        //     var reportViewer = _reportLoaderService.Load(reportLoaderInfo);
        //     var viewerWindow = ContainerLocator.Current.Resolve<ModuleReportViewerView>();
        //     if (viewerWindow == null) return;
        //     if (viewerWindow.DataContext is ReportViewerViewModelBase viewModel)
        //         viewModel.ViewReport(reportLoaderInfo, reportViewer, viewerWindow);
        //     viewerWindow.Show();
        //     var window = viewerWindow.ParentOfType<Window>();
        //     window.ShowInTaskbar = true;
        // }
    }

    public class Filter
    {
        public Action<string, string> Action { get; set; }
        public string Field { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}