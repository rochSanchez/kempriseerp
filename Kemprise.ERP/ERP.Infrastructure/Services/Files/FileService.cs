﻿using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.Services.Notifications;
using ERP.WebApi.DTO.DTOs.Files;
using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Prism.Services.Dialogs;

namespace ERP.Infrastructure.Services.Files
{
    public interface IFileService
    {
        IEnumerable<FileDto> GetAnyFile();

        IEnumerable<FileDto> GetImageFile();

        IEnumerable<FileDto> GetPdfFile();

        void SaveFile(FileDto fileDto);
    }

    public class FileService : IFileService
    {
        private readonly IDesktopNotificationService _desktopNotificationService;

        public FileService(IDesktopNotificationService desktopNotificationService)
        {
            _desktopNotificationService = desktopNotificationService;
        }

        public IEnumerable<FileDto> GetAnyFile()
        {
            return ShowDialog("Files | *.jpg; *.jpeg; *.pdf; *.docx; *.doc; *.xlsx; *.xls; ");
        }

        public IEnumerable<FileDto> GetImageFile()
        {
            return ShowDialog("JPEG | *.jpg; *.jpeg; ");
        }

        public IEnumerable<FileDto> GetPdfFile()
        {
            return ShowDialog("PDF | *.pdf; ");
        }

        private static IEnumerable<FileDto> ShowDialog(string fileFilters)
        {
            var dialog = new OpenFileDialog
            {
                Filter = fileFilters,
                Multiselect = true
            };
            dialog.ShowDialog();

            if (!dialog.FileNames.Any())
                return null;

            var files = new List<FileDto>();
            dialog.FileNames.ForEach(file =>
            {
                files.Add(new FileDto
                {
                    FileName = Path.GetFileNameWithoutExtension(file),
                    FileFormat = Path.GetExtension(file).ToLower(),
                    FileData = File.ReadAllBytes(file)
                });
            });

            return files;
        }

        public void SaveFile(FileDto fileDto)
        {
            var saveDialog = new SaveFileDialog
            {
                OverwritePrompt = true,
                Filter = SaveAsType(fileDto.FileFormat),
                FileName = fileDto.FileName
            };

            if (saveDialog.ShowDialog() == false) return;
            if (string.IsNullOrEmpty(saveDialog.FileName)) return;

            File.WriteAllBytes(saveDialog.FileName, fileDto.FileData);

            var notificationInfo = new NotificationInfo
            {
                ModuleName = "FILE NOTIFICATION",
                Message = "File Successfully Saved"
            };
            var successfulAlert = new SaveSuccessfulAlert(notificationInfo);
            _desktopNotificationService.NotifyUser(successfulAlert);
        }

        private static string SaveAsType(string format)
        {
            return format switch
            {
                ".docx" => $"Document |*{format}",
                ".doc" => $"Document |*{format}",
                ".pdf" => $"PDF |*{format}",
                ".xls" => $"Excel |*{format}",
                ".xlsx" => $"Excel |*{format}",
                ".jpg" => $"JPEG |*{format}",
                ".jpeg" => $"JPEG |*{format}",
                _ => $"FILES |*{format}"
            };
        }
    }
}