﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.Lookups;

namespace ERP.Infrastructure.Services.Lookup
{
    public interface ILookupService
    {
        Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookupAsync<T>(ApiControllers apiController,
            string method) where T : Lookups;

        void RegisterLookup<T>(ApiControllers apiController, string method) where T : Lookups;

        Task<Dictionary<string, IEnumerable<LookupEntity>>> GetLookupsAsync();
    }
}