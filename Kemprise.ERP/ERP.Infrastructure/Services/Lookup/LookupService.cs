﻿using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.Services.APICommon;
using ERP.WebApi.DTO.Lookups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Infrastructure.Services.Lookup
{
    public class LookupService : ILookupService
    {
        public static Dictionary<string, IEnumerable<LookupEntity>> LookupRepository =
            new Dictionary<string, IEnumerable<LookupEntity>>();

        private readonly IApiRequestManager _requestManager;
        private readonly List<Func<Task<Lookups>>> _setLookup;

        public LookupService(IApiRequestManager requestManager)
        {
            _requestManager = requestManager;
            _setLookup = new List<Func<Task<Lookups>>>();
        }

        public bool HasError { get; private set; }

        public void RegisterLookup<T>(ApiControllers apiController, string method) where T : Lookups
        {
            _setLookup.Add(async () => await _requestManager.GetLookups<T>(apiController, method));
        }

        public async Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookupAsync<T>(
            ApiControllers apiController, string method) where T : Lookups
        {
            var lookups = new Dictionary<string, IEnumerable<LookupEntity>>();
            var result = await _requestManager.GetLookups<T>(apiController, method);

            var propertyInfos = result.GetType().GetProperties();
            propertyInfos.ForEach(info =>
                lookups.Add(info.Name, info.GetValue(result, null).As<IEnumerable<LookupEntity>>()));

            lookups.ForEach(lookup =>
            {
                var data = lookup.Value.OrderBy(item => item.Text).ToList();

                if (LookupRepository.ContainsKey(lookup.Key))
                    LookupRepository[lookup.Key] = data;
                else
                    LookupRepository.Add(lookup.Key, data);
            });

            foreach (var lookup in lookups)
                if (!lookup.Value.Any())
                    HasError = true;

            return LookupRepository;
        }

        public async Task<Dictionary<string, IEnumerable<LookupEntity>>> GetLookupsAsync()
        {
            var lookups = new Dictionary<string, IEnumerable<LookupEntity>>();
            foreach (var action in _setLookup)
            {
                try
                {
                    var result = await action.Invoke();
                    if (result == null) continue;

                    var propertyInfos = result.GetType().GetProperties();
                    propertyInfos.ForEach(info =>
                        lookups.Add(info.Name, info.GetValue(result, null).As<IEnumerable<LookupEntity>>()));

                    lookups.ForEach(lookup =>
                    {
                        var data = lookup.Value.OrderBy(item => item.Text).ToList();

                        if (LookupRepository.ContainsKey(lookup.Key))
                            LookupRepository[lookup.Key] = data;
                        else
                            LookupRepository.Add(lookup.Key, data);
                    });

                    foreach (var lookup in lookups)
                        if (!lookup.Value.Any())
                            HasError = true;
                }
                catch (Exception)
                {
                    HasError = true;
                    throw;
                }
            }

            return LookupRepository;
        }
    }
}