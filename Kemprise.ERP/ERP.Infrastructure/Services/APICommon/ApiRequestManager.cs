﻿using ERP.WebApi.DTO.Security.Authentication;
using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ERP.WebApi.DTO.Lookups;

namespace ERP.Infrastructure.Services.APICommon
{
    public class ApiRequestManager : IApiRequestManager
    {
        private readonly string baseUrl = "https://localhost:44312/api";
        // private readonly string baseUrl = "https://kemprisetestwebapp.azurewebsites.net/api";

        public async Task<ApiLoginResult> LoginAsync(string username, string userPassword)
        {
            var result = new ApiLoginResult();
            try
            {
                var response = await baseUrl
                    .AppendPathSegment(nameof(ApiControllers.Account))
                    .AppendPathSegment("authenticate").PostJsonAsync(new
                    {
                        userName = username,
                        password = userPassword
                    }).ReceiveJson<AuthenticationResponse>();

                ApiSession.SetId(response.Id);
                ApiSession.SetToken(response.Token);
                ApiSession.SetUserName(response.UserName);
                ApiSession.SetEmail(response.Email);
                ApiSession.SetRoles(response.Roles);
            }
            catch (FlurlHttpException e)
            {
                if (e.Call.Response != null)
                {
                    var response = e.Call.Response.GetJsonAsync<ApiLoginError>();
                    result.StatusCode = e.Call.Response.StatusCode;
                }
                result.ErrorMessage = await e.GetResponseStringAsync() ?? "Connection Error";
                result.IsSuccessful = false;
            }

            return result;
        }

        public async Task<T> GetRecordAsync<T>(ApiControllers controller, string method, object[] parameters) where T : class
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .GetJsonAsync<T>();
            return result;
        }

        public async Task<IEnumerable<T>> GetRecordsAsync<T>(ApiControllers controller, string method,
            object[] parameters) where T : class
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .GetJsonAsync<IEnumerable<T>>();
            return result;
        }

        public async Task<T> GetLookups<T>(ApiControllers controller, string method) where T : Lookups
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .GetJsonAsync<T>();
            return result;
        }

        public async Task<T> AddRecordAsync<T>(ApiControllers controller, string method, T data, object[] parameters) where T : class
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .PostJsonAsync(data)
                .ReceiveJson<T>();
            return result;
        }

        public async Task<T> AddRecordAsync<T>(ApiControllers controller, string method, Dictionary<string, object> data, object[] parameters) where T : class
        {
            try
            {
                var result = await baseUrl
                    .AppendPathSegment(controller.ToString())
                    .AppendPathSegment(method)
                    .AppendCallParameters(parameters)
                    .WithOAuthBearerToken(ApiSession.GetToken())
                    .PostJsonAsync(data)
                    .ReceiveJson<T>();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task UpdateRecordAsync<T>(ApiControllers controller, string method, T data, object[] parameters) where T : class
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .PostJsonAsync(data);
        }

        public async Task UpdateRecordAsync(ApiControllers controller, string method, Dictionary<string, object> data, object[] parameters)
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .PostJsonAsync(data);
        }

        public async Task<IEnumerable<T>> UpdateRecordAsync<T>(ApiControllers controller, string method, Dictionary<string, object> data, object[] parameters)
        {
            var result = await baseUrl
                .AppendPathSegment(controller.ToString())
                .AppendPathSegment(method)
                .AppendCallParameters(parameters)
                .WithOAuthBearerToken(ApiSession.GetToken())
                .PostJsonAsync(data)
                .ReceiveJson<IEnumerable<T>>();
            return result;
        }

        public async Task UpdateRecordMultiPartAsync(ApiControllers controller, string method, MultipartFormDataContent data, object[] parameters)
        {
            try
            {
                await baseUrl
                    .AppendPathSegment(controller.ToString())
                    .AppendPathSegment(method)
                    .AppendCallParameters(parameters)
                    .WithOAuthBearerToken(ApiSession.GetToken())
                    .PostMultipartAsync(mp => mp.Add(data));
            }
            catch (Exception a)
            {
                Console.WriteLine(a);
                throw;
            }
        }
    }
}