﻿using Flurl;

namespace ERP.Infrastructure.Services.APICommon
{
    public static class FlurlExtensions
    {
        public static Url AppendCallParameters(this Url url, params object[] parameters)
        {
            if (parameters == null) return url;
            foreach (var parameter in parameters) url.AppendPathSegment(parameter);
            return url;
        }
    }
}