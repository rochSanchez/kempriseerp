﻿namespace ERP.Infrastructure.Services.APICommon
{
    public class ApiLoginError
    {
        public string Error { get; set; }
    }
}