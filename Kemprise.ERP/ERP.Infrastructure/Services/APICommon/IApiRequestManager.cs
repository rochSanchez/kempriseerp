﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ERP.WebApi.DTO.Lookups;

namespace ERP.Infrastructure.Services.APICommon
{
    public interface IApiRequestManager
    {
        Task<ApiLoginResult> LoginAsync(string userName, string password);

        Task<T> GetRecordAsync<T>(ApiControllers controller, string method, object[] parameter) where T : class;

        Task<IEnumerable<T>> GetRecordsAsync<T>(ApiControllers controller, string method, object[] parameters = null) where T : class;

        Task<T> GetLookups<T>(ApiControllers controller, string method) where T : Lookups;

        Task<T> AddRecordAsync<T>(ApiControllers controller, string method, T data, object[] parameters) where T : class;

        Task<T> AddRecordAsync<T>(ApiControllers controller, string method, Dictionary<string, object> data,
            object[] parameters) where T : class;

        Task UpdateRecordAsync<T>(ApiControllers controller, string method, T data, object[] parameters)
            where T : class;

        Task UpdateRecordAsync(ApiControllers controller, string method, Dictionary<string, object> data,
            object[] parameters);

        Task<IEnumerable<T>> UpdateRecordAsync<T>(ApiControllers controller, string method,
            Dictionary<string, object> data, object[] parameters);

        Task UpdateRecordMultiPartAsync(ApiControllers controller, string method, MultipartFormDataContent data,
            object[] parameters);
    }
}