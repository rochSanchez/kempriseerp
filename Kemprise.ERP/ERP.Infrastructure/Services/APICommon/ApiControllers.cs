﻿namespace ERP.Infrastructure.Services.APICommon
{
    public enum ApiControllers
    {
        Account,
        Customer,
        Employee,
        Vendor
    }
}