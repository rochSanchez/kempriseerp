﻿namespace ERP.Infrastructure.Services.APICommon
{
    public class ApiLoginResult
    {
        public ApiLoginResult()
        {
            IsSuccessful = true;
        }

        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public int StatusCode { get; set; }
    }
}