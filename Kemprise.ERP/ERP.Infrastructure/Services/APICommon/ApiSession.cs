﻿using ERP.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;

namespace ERP.Infrastructure.Services.APICommon
{
    public static class ApiSession
    {
        private static readonly SecureString Id = new SecureString();
        private static readonly SecureString UserName = new SecureString();
        private static readonly SecureString Email = new SecureString();
        private static readonly SecureString Token = new SecureString();
        private static readonly SecureString LocationId = new SecureString();
        private static List<string> _roles = new List<string>();

        public static string GetId()
        {
            return Decode(Token);
        }

        public static void SetId(string id)
        {
            Token.Clear();
            foreach (var c in id) Token.AppendChar(c);
        }

        public static string GetToken()
        {
            return Decode(Token);
        }

        public static void SetToken(string token)
        {
            Token.Clear();
            foreach (var c in token) Token.AppendChar(c);
        }

        public static string GetUserName()
        {
            return Decode(UserName);
        }

        public static void SetUserName(string userName)
        {
            UserName.Clear();
            foreach (var c in userName) UserName.AppendChar(c);
        }

        public static string GetEmail()
        {
            return Decode(Email);
        }

        public static void SetEmail(string password)
        {
            Email.Clear();
            foreach (var c in password) Email.AppendChar(c);
        }

        private static string Decode(SecureString secure)
        {
            var unmanagedPassword = Marshal.SecureStringToBSTR(secure);
            var decoded = Marshal.PtrToStringUni(unmanagedPassword);
            Marshal.ZeroFreeBSTR(unmanagedPassword);

            return decoded;
        }

        public static void SetRoles(IEnumerable<string> responseRoles)
        {
            _roles.Clear();
            responseRoles.ForEach(item => _roles.Add(item));
        }

        public static bool HasRole(string role)
        {
            return _roles.Contains(role);
        }
    }
}