﻿namespace ERP.Infrastructure.Services.Notifications
{
    public interface INotificationType
    {
        string Header { get; }
        string Content { get; }
        string Icon { get; }
    }

    public class SaveSuccessfulAlert : INotificationType
    {
        public SaveSuccessfulAlert(NotificationInfo notificationInfo)
        {
            Header = $"{notificationInfo.ModuleName}";
            Content = "Save Successful.";
        }

        public string Header { get; }
        public string Content { get; }
        public string Icon => "SavedAlertIcon";
    }
}