﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace ERP.Infrastructure.Services.Notifications
{
    public interface IDesktopNotificationService
    {
        void NotifyUser(INotificationType notificationType);
    }

    public class DesktopNotificationService : IDesktopNotificationService
    {
        private readonly RadDesktopAlertManager _alertManager;

        public DesktopNotificationService()
        {
            _alertManager = new RadDesktopAlertManager();
        }

        public void NotifyUser(INotificationType notificationType)
        {
            var radDesktopAlert = new RadDesktopAlert
            {
                Content = notificationType.Content,
                Header = notificationType.Header,
                ShowCloseButton = true,
                CanAutoClose = true,
                ShowDuration = 2000,
                Icon = new Image
                {
                    Source = Application.Current.FindResource(notificationType.Icon) as ImageSource,
                    Width = 80,
                    Height = 80
                }
            };

            _alertManager.ShowAlert(radDesktopAlert);
        }
    }
}