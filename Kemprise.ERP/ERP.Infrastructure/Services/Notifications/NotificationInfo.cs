﻿namespace ERP.Infrastructure.Services.Notifications
{
    public class NotificationInfo
    {
        public string ModuleName { get; set; }
        public string Message { get; set; }
    }
}