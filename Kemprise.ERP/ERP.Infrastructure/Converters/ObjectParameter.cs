﻿namespace ERP.Infrastructure.Converters
{
    public class ObjectParameter
    {
        public object NewValue { get; set; }
        public string PropertyName { get; set; }
    }
}