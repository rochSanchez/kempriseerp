﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ERP.Infrastructure.Converters
{
    public class RadioButtonIsCheckedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || parameter == DependencyProperty.UnsetValue)
                return false;
            if (string.IsNullOrEmpty(value?.ToString()) || string.IsNullOrEmpty(parameter?.ToString()))
                return false;
            return value.ToString() == parameter.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}