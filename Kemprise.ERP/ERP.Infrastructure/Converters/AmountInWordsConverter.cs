﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ERP.Infrastructure.Converters
{
    public static class AmountInWordsConverter
    {
        public static string ConvertToWords(decimal number)
        {
            if (number == 0) return "";
            string numberInWords;

            var valueToConvert = number.ToString(CultureInfo.InvariantCulture)
                .Substring(0, number.ToString(CultureInfo.InvariantCulture).IndexOf('.', 0));
            var numberLength = valueToConvert.Length;

            var decimalvalue = number.ToString(CultureInfo.InvariantCulture)
                .Substring(number.ToString(CultureInfo.InvariantCulture).Length - 2);
            var decimalWord = Convert.ToInt16(decimalvalue) != 00 ? ConvertToOnes(decimalvalue) + " Kobo Only" : "";

            if (numberLength <= 2)
                numberInWords = ConvertToOnes(valueToConvert);
            else if (numberLength == 3)
                numberInWords = ConvertToHundreds(valueToConvert);
            else if (numberLength >= 4 && numberLength <= 6)
                numberInWords = ConvertToThousands(valueToConvert);
            else if (numberLength >= 7 && numberLength <= 9)
                numberInWords = ConvertToMillions(valueToConvert);
            else
                numberInWords = ConvertToBillions(valueToConvert);

            return string.IsNullOrWhiteSpace(numberInWords) ? decimalWord : numberInWords + " Naira " + decimalWord;
        }

        public static string ToWords(this decimal number)
        {
            string numberInWords;
            var decimalWord = "";

            var valueToConvert = number.ToString(CultureInfo.InvariantCulture).Contains(".")
                ? number.ToString(CultureInfo.InvariantCulture)
                    .Substring(0, number.ToString(CultureInfo.InvariantCulture).IndexOf('.', 0))
                : number.ToString(CultureInfo.InvariantCulture);
            var numberLength = valueToConvert.Length;

            if (number.ToString(CultureInfo.InvariantCulture).Contains("."))
            {
                var x = Math.Round(number, 2);
                var decimalvalue = x.ToString(CultureInfo.InvariantCulture)
                    .Substring(x.ToString(CultureInfo.InvariantCulture).IndexOf(".", StringComparison.Ordinal) + 1);
                if (decimalvalue.Length == 1)
                    decimalvalue = decimalvalue + "0";
                decimalWord = Convert.ToInt16(decimalvalue) != 00 ? ConvertToOnes(decimalvalue) : "";
            }

            if (numberLength <= 2)
                numberInWords = ConvertToOnes(valueToConvert);
            else if (numberLength == 3)
                numberInWords = ConvertToHundreds(valueToConvert);
            else if (numberLength >= 4 && numberLength <= 6)
                numberInWords = ConvertToThousands(valueToConvert);
            else if (numberLength >= 7 && numberLength <= 9)
                numberInWords = ConvertToMillions(valueToConvert);
            else
                numberInWords = ConvertToBillions(valueToConvert);

            return string.IsNullOrWhiteSpace(numberInWords)
                ? decimalWord
                : numberInWords + (string.IsNullOrEmpty(decimalWord) ? "" : " and " + decimalWord);
        }

        private static string ConvertToOnes(string number)
        {
            var value = Convert.ToInt32(number) <= 19
                ? Ones.GetWordValue(Convert.ToInt32(number))
                : ConvertToTens(number);
            return value;
        }

        private static string ConvertToTens(string number)
        {
            var firstSet = number.Substring(0, 1);
            var secondSet = number.Substring(1);

            var value1 = Tens.GetWordValue(Convert.ToInt32(firstSet));
            var value2 = Ones.GetWordValue(Convert.ToInt32(secondSet));

            return value1 + " " + value2;
        }

        private static string ConvertToHundreds(string number)
        {
            if (number == "000") return null;

            var firstSet = number.Substring(0, 1);
            var secondSet = number.Substring(1);

            var value1 = Ones.GetWordValue(Convert.ToInt32(firstSet));
            var value2 = ConvertToOnes(secondSet);
            return string.IsNullOrWhiteSpace(value1)
                ? value2
                : string.IsNullOrWhiteSpace(value2)
                    ? value1 + " Hundred"
                    : value1 + " Hundred and " + value2;
        }

        private static string ConvertToThousands(string number)
        {
            if (number == "000000") return null;

            var firstSet = number.Length == 6
                ? number.Substring(0, 3)
                : number.Substring(0, number.Length % 3);

            var secondSet = number.Length == 6
                ? number.Substring(3)
                : number.Substring(number.Length % 3);

            var value1 = firstSet.Length == 3
                ? ConvertToHundreds(firstSet == "" ? "0" : firstSet)
                : ConvertToOnes(firstSet == "" ? "0" : firstSet);
            var value2 = ConvertToHundreds(secondSet);

            return string.IsNullOrWhiteSpace(value1)
                ? value2
                : string.IsNullOrWhiteSpace(value1) && string.IsNullOrWhiteSpace(value2)
                    ? null
                    : string.IsNullOrWhiteSpace(value2)
                        ? value1 + " Thousand"
                        : number.Length > 3 || string.IsNullOrWhiteSpace(value1)
                            ? value1 + " Thousand, " + value2
                            : value2 + " Thousand, ";
        }

        private static string ConvertToMillions(string number)
        {
            if (number == "000000000") return null;

            var firstSet = number.Length == 9
                ? number.Substring(0, 3)
                : number.Substring(0, number.Length % 6);
            var secondSet = number.Length == 9
                ? number.Substring(3)
                : number.Substring(number.Length % 6);

            var value1 = firstSet.Length == 3
                ? ConvertToHundreds(firstSet)
                : ConvertToOnes(firstSet);
            var value2 = ConvertToThousands(secondSet);

            return string.IsNullOrWhiteSpace(value1) && string.IsNullOrWhiteSpace(value2)
                ? null
                : string.IsNullOrWhiteSpace(value1)
                    ? value2
                    : string.IsNullOrWhiteSpace(value2)
                        ? value1 + " Million"
                        : value1 + " Million, " + value2;
        }

        private static string ConvertToBillions(string number)
        {
            var firstSet = number.Length == 12
                ? number.Substring(0, 3)
                : number.Substring(0, number.Length % 9);
            var secondSet = number.Length == 9
                ? number.Substring(3)
                : number.Substring(number.Length % 9);

            var value1 = firstSet.Length == 3
                ? ConvertToHundreds(firstSet)
                : ConvertToOnes(firstSet);
            var value2 = ConvertToMillions(secondSet);

            return string.IsNullOrWhiteSpace(value2) ? value1 + " Billion" : value1 + " Billion, " + value2;
        }
    }

    public static class Ones
    {
        private static Dictionary<int, string> _ones;

        public static string GetWordValue(int key)
        {
            _ones = new Dictionary<int, string>
            {
                {1, "One"},
                {2, "Two"},
                {3, "Three"},
                {4, "Four"},
                {5, "Five"},
                {6, "Six"},
                {7, "Seven"},
                {8, "Eight"},
                {9, "Nine"},
                {10, "Ten"},
                {11, "Eleven"},
                {12, "Twelve"},
                {13, "Thirteen"},
                {14, "Fourteen"},
                {15, "Fifteen"},
                {16, "Sixteen"},
                {17, "Seventeen"},
                {18, "Eighteen"},
                {19, "Nineteen"},
                {20, ""},
                {0, null}
            };

            return _ones[key];
        }
    }

    public static class Tens
    {
        private static Dictionary<int, string> _tens;

        public static string GetWordValue(int key)
        {
            _tens = new Dictionary<int, string>
            {
                {2, "Twenty"},
                {3, "Thirty"},
                {4, "Fourty"},
                {5, "Fifty"},
                {6, "Sixty"},
                {7, "Seventy"},
                {8, "Eighty"},
                {9, "Ninety"}
            };

            return _tens[key];
        }
    }
}