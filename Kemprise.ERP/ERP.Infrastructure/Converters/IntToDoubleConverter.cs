﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ERP.Infrastructure.Converters
{
    public class IntToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var returnValue = System.Convert.ToDouble(value);
            return returnValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var returnValue = System.Convert.ToInt32(value);
            return returnValue;
        }
    }
}