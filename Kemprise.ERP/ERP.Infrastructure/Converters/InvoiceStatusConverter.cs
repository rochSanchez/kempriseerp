﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ERP.Infrastructure.Converters
{
    public class InvoiceStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var x = value != null && (bool)value;
            return x ? Brushes.Green : Brushes.DarkRed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}