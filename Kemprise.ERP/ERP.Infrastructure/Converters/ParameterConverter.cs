﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ERP.Infrastructure.Converters
{
    public class ParameterConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || parameter == DependencyProperty.UnsetValue)
                return new[] { Binding.DoNothing };
            var propertyName = values[0].ToString();
            if (parameter == null || string.IsNullOrEmpty(propertyName))
                return new[] { Binding.DoNothing };

            return new ObjectParameter { NewValue = parameter, PropertyName = propertyName };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new[] { Binding.DoNothing };
        }
    }
}