﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ERP.Infrastructure.Converters
{
    public class BoolToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? "No" : (bool)value ? "Yes" : "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? "No" : (bool)value ? "Yes" : "No";
        }
    }
}