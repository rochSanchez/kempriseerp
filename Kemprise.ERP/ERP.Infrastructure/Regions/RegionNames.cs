﻿namespace ERP.Infrastructure.Regions
{
    public static class RegionNames
    {
        public static string DialogRegion = "DialogRegion";
        public static string QuickAccessRegion = "QuickAccessRegion";
        public static string MainWorkAreaRegion = "MainWorkAreaRegion";
    }
}