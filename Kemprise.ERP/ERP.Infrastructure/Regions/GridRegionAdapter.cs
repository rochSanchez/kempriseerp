﻿using Prism.Regions;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

namespace ERP.Infrastructure.Regions
{
    public class GridRegionAdapter : RegionAdapterBase<Grid>
    {
        public GridRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory) : base(regionBehaviorFactory)
        {
        }

        protected override void Adapt(IRegion region, Grid regionTarget)
        {
            region.Views.CollectionChanged += (s, e) =>
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                    foreach (FrameworkElement frameworkElement in e.NewItems)
                        regionTarget.Children.Add(frameworkElement);

                if (e.Action != NotifyCollectionChangedAction.Remove) return;
                {
                    foreach (FrameworkElement frameworkElement in e.OldItems)
                        regionTarget.Children.Remove(frameworkElement);
                }
            };
        }

        protected override IRegion CreateRegion()
        {
            return new AllActiveRegion();
        }
    }
}