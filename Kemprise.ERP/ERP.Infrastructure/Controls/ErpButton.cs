﻿using System.Windows;
using System.Windows.Controls;

namespace ERP.Infrastructure.Controls
{
    public class ErpButton : Button
    {
        static ErpButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ErpButton), new FrameworkPropertyMetadata(typeof(ErpButton)));
            // FillDataProperty = DependencyProperty.Register("FillData", typeof(Geometry), typeof(ErpButton));
        }

        // public static readonly DependencyProperty FillDataProperty;
        //
        // public Geometry FillData
        // {
        //     get => (Geometry)GetValue(FillDataProperty);
        //     set => SetValue(FillDataProperty, value);
        // }
    }
}