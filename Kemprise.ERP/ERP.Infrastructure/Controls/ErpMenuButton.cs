﻿using System.Windows;
using System.Windows.Controls;

namespace ERP.Infrastructure.Controls
{
    public class ErpMenuButton : Button
    {
        static ErpMenuButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ErpMenuButton), new FrameworkPropertyMetadata(typeof(ErpMenuButton)));
            IsSelectedProperty = DependencyProperty.Register("IsSelected", typeof(bool), typeof(ErpMenuButton));
        }

        public static readonly DependencyProperty IsSelectedProperty;

        public bool IsSelected
        {
            get => (bool)GetValue(IsSelectedProperty);
            set => SetValue(IsSelectedProperty, value);
        }
    }
}