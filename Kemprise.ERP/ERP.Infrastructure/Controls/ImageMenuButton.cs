﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ERP.Infrastructure.Controls
{
    public class ImageMenuButton : Button
    {
        static ImageMenuButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageMenuButton), new FrameworkPropertyMetadata(typeof(ImageMenuButton)));
            ImageSourceProperty = DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImageMenuButton));
        }

        public static readonly DependencyProperty ImageSourceProperty;

        public ImageSource ImageSource
        {
            get => (ImageSource)GetValue(ImageSourceProperty);
            set => SetValue(ImageSourceProperty, value);
        }
    }
}