﻿using Prism.Events;
using Prism.Regions;
using Unity;

namespace ERP.Infrastructure.Controllers
{
    public class ControllerBase : IControllerBase
    {
        protected ControllerBase(IEventAggregator eventAggregator, IRegionManager regionManager,
            IUnityContainer container)
        {
            EventAggregator = eventAggregator;
            RegionManager = regionManager;
            Container = container;
        }

        public IUnityContainer Container { get; }

        public IEventAggregator EventAggregator { get; }

        public IRegionManager RegionManager { get; }
    }
}