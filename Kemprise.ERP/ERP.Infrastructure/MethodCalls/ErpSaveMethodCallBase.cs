﻿using ERP.Infrastructure.Modules;
using ERP.Infrastructure.Services.APICommon;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Infrastructure.MethodCalls
{
    public abstract class ErpSaveMethodCallBase : IErpSaveMethodCall
    {
        private readonly IApiRequestManager _apiRequestManager;

        protected ErpSaveMethodCallBase(IApiRequestManager apiRequestManager)
        {
            _apiRequestManager = apiRequestManager;
            ShowNotification = true;
        }

        public virtual string BusyContent => "Saving...";
        public abstract string Category { get; }
        public bool IsSuccessful { get; set; }
        public abstract string MethodName { get; }
        public string MethodParameter { get; set; }
        public abstract ErpModules ModuleName { get; }
        public abstract ApiControllers Controller { get; }
        public abstract bool UseDefaultParameter { get; }
        public bool ShowNotification { get; set; }

        public abstract Task SaveRecordAsync(EntityBase entity);

        protected async Task<T> SaveNewRecordAsync<T>(string method, Dictionary<string, object> entity = default, object[] parameters = null) where T : class
        {
            return await _apiRequestManager.AddRecordAsync<T>(Controller, method, entity, parameters);
        }

        protected async Task<T> SaveNewRecordAsync<T>(string method, T entity = default, object[] parameters = null) where T : class
        {
            return await _apiRequestManager.AddRecordAsync(Controller, method, entity, parameters);
        }

        protected async Task UpdateRecordAsync<T>(string method, T entity = default, object[] parameters = null) where T : class
        {
            await _apiRequestManager.UpdateRecordAsync(Controller, method, entity, parameters);
        }

        protected async Task UpdateRecordAsync(string method, Dictionary<string, object> entity = default, object[] parameters = null)
        {
            await _apiRequestManager.UpdateRecordAsync(Controller, method, entity, parameters);
        }

        protected async Task<IEnumerable<T>> UpdateRecordAsync<T>(string method, Dictionary<string, object> entity = null, object[] parameters = null)
        {
            return await _apiRequestManager.UpdateRecordAsync<T>(Controller, method, entity, parameters);
        }
    }
}