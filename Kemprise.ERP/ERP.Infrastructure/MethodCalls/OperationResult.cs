﻿namespace ERP.Infrastructure.MethodCalls
{
    public enum OperationResult
    {
        Success,
        Failed
    }
}