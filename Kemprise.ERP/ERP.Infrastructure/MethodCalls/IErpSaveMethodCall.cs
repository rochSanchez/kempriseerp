﻿using System.Threading.Tasks;

namespace ERP.Infrastructure.MethodCalls
{
    public interface IErpSaveMethodCall : IErpMethodCall
    {
        Task SaveRecordAsync(EntityBase entity);
    }
}