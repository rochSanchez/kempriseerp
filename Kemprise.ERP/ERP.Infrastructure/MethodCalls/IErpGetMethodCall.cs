﻿using ERP.Infrastructure.PropertyChangeNotification;
using System;
using System.Threading.Tasks;

namespace ERP.Infrastructure.MethodCalls
{
    public interface IErpGetMethodCall : IErpMethodCall
    {
        Action WhenDone { get; set; }

        bool RunAtStartup { get; }

        Task RunAsync(DynamicNotifyPropertyChangedProxy entity);
    }
}