﻿using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.Infrastructure.MethodCalls
{
    public abstract class GetMethodCallBase : IErpGetMethodCall
    {
        private readonly IApiRequestManager _requestManager;

        protected GetMethodCallBase(IApiRequestManager requestManager)
        {
            _requestManager = requestManager;
        }

        public abstract ApiControllers Controller { get; }
        public virtual string BusyContent => "Loading...";
        public string MethodParameter { get; set; }
        public bool IsSuccessful { get; set; }
        public Action WhenDone { get; set; }
        public abstract string MethodName { get; }
        public abstract ErpModules ModuleName { get; }
        public abstract string Category { get; }
        public abstract bool RunAtStartup { get; }
        public abstract bool UseDefaultParameter { get; }

        public abstract Task RunAsync(DynamicNotifyPropertyChangedProxy entity);

        protected async Task<T> GetRecordAsync<T>(string method, object[] parameter = null) where T : class
        {
            return await _requestManager.GetRecordAsync<T>(Controller, method, parameter);
        }

        protected async Task<IEnumerable<T>> GetRecordsAsync<T>(string method, object[] parameter = null) where T : class
        {
            return await _requestManager.GetRecordsAsync<T>(Controller, method, parameter);
        }
    }
}