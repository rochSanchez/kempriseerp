﻿using ERP.Infrastructure.Modules;

namespace ERP.Infrastructure.MethodCalls
{
    public interface IErpMethodCall
    {
        string BusyContent { get; }
        string MethodParameter { get; set; }
        bool IsSuccessful { get; }
        string MethodName { get; }
        ErpModules ModuleName { get; }
        bool UseDefaultParameter { get; }
        string Category { get; }
    }
}