﻿namespace ERP.Infrastructure.MethodCalls
{
    public class OperationResponse
    {
        public OperationResponse(string methodName, OperationResult operationResult, string errorMessage = "")
        {
            OperationResult = operationResult;
            ErrorMessage = errorMessage;
            MethodName = methodName;
        }

        public string ErrorMessage { get; }
        public string MethodName { get; }
        public OperationResult OperationResult { get; }
    }
}