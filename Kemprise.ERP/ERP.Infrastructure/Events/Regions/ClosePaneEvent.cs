﻿using ERP.Infrastructure.ViewModel;
using Prism.Events;

namespace ERP.Infrastructure.Events.Regions
{
    public class ClosePaneEvent<T> : PubSubEvent<ClosePanePayload> where T : ErpViewModelBase
    {
    }

    public class ClosePanePayload
    {
        public ClosePanePayload(object view, string typeName = "")
        {
            View = view;
            TypeName = typeName;
        }

        public object View { get; }
        public string TypeName { get; }
    }
}