﻿using Prism.Events;

namespace ERP.Infrastructure.Events.Errors
{
    public class ErrorEvent : PubSubEvent<ErrorEventPayload>
    {
    }

    public class ErrorEventPayload
    {
        public ErrorEventPayload(string message = null)
        {
            Message = message;
        }

        public string Message { get; }
    }
}