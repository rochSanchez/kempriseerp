﻿using ERP.Infrastructure.Modules;
using Prism.Events;

namespace ERP.Infrastructure.Events.Modules
{
    public class OpenModuleEvent : PubSubEvent<OpenModuleEventPayload>
    {
    }

    public class OpenModuleEventPayload
    {
        public ErpModules ErpModule { get; }
        public IQuickAccess QuickAccess { get; }

        public OpenModuleEventPayload(ErpModules erpModule, IQuickAccess quickAccess)
        {
            ErpModule = erpModule;
            QuickAccess = quickAccess;
        }
    }
}