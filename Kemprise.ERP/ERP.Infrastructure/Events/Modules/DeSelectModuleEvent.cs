﻿using ERP.Infrastructure.Controllers;
using Prism.Events;

namespace ERP.Infrastructure.Events.Modules
{
    public class DeSelectModuleEvent<T> : PubSubEvent where T : IControllerBase
    {
    }
}