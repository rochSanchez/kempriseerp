﻿using Prism.Events;

namespace ERP.Infrastructure.Events.Modules
{
    public class RefreshItemListEvent : PubSubEvent<RefreshItemListEventPayload>
    {
    }

    public class RefreshItemListEventPayload
    {
        public RefreshItemListEventPayload(object viewModel, string typeName)
        {
            ViewModel = viewModel;
            TypeName = typeName;
        }

        public object ViewModel { get; }
        public string TypeName { get; }
    }
}