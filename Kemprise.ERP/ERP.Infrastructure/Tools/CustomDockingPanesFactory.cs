﻿using System.Linq;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Docking;

namespace ERP.Infrastructure.Tools
{
    public class CustomDockingPanesFactory : DockingPanesFactory
    {
        protected override void AddPane(RadDocking radDocking, RadPane pane)
        {
            var result = radDocking.SplitItems.Where(item => item is RadPaneGroup);
            var paneGroups = result.OfType<RadPaneGroup>();
            var docPanes = paneGroups.Select(radPaneGroup => radPaneGroup.Items.OfType<RadDocumentPane>());
            var any = docPanes.Any(panes => panes.Any(radDocumentPane => radDocumentPane.Tag == pane.Tag));
            if (any) return;
            var docHost = radDocking.SplitItems.ToList().FirstOrDefault(i => i.Control.Name.Contains("DocPanes"));
            if (docHost is RadPaneGroup paneGroup)
                paneGroup.Items.Add(pane);
            else
                base.AddPane(radDocking, pane);
        }

        protected override void RemovePane(RadPane pane)
        {
            if (pane == null) return;
            pane.Header = null;
            pane.Content = null;
            pane.DataContext = null;
            pane.RemoveFromParent();
        }
    }
}