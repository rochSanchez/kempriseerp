﻿using System.Globalization;

namespace ERP.Infrastructure.Extensions
{
    public static class StringHelpers
    {
        public static string WithQuote(this string value)
        {
            return string.Format("'{0}'", value);
        }

        public static string NewParameter()
        {
            return "'0'";
        }

        public static string NumericZero()
        {
            return "0";
        }

        public static string ToSentence(this string str)
        {
            var textInfo = new CultureInfo("en-US", false).TextInfo;

            var result = textInfo.ToTitleCase(str.ToLower());
            return result;
        }
    }
}