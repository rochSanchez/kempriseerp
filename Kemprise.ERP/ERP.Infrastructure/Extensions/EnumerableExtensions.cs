﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ERP.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            if (items == null)
                return;
            foreach (T obj in items)
                action(obj);
        }

        public static IEnumerable<T> Remove<T>(this IEnumerable<T> items, Func<T, bool> func)
        {
            List<T> list = items.ToList<T>();
            ((IEnumerable<T>)list).Where<T>(func).ToList<T>().ForEach((Action<T>)(i => list.Remove(i)));
            return (IEnumerable<T>)list;
        }
    }
}