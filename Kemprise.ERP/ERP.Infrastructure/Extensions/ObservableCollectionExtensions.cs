﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ERP.Infrastructure.Extensions
{
    public static class ObservableCollectionExtensions
    {
        public static void SortByPropertyName<T>(this ICollection<T> collection, string propertyName)
        {
            var propertyValue = typeof(T).GetProperty(propertyName);
            if (propertyValue == null) return;

            var sortedCollection = new ObservableCollection<T>(collection.OrderBy(x => propertyValue.GetValue(x, null)));
            collection.Clear();

            sortedCollection.ForEach(collection.Add);
        }
    }
}