﻿using System;

namespace ERP.Infrastructure.Extensions
{
    public static class DateTimeExtensions
    {
        public enum Type
        {
            Of = 1,
            DayOf = 2,
            DaySuffix = 3
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0) diff += 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static string ToWords(this DateTime? dateTime, Type type)
        {
            if (dateTime == null) return string.Empty;
            var day = string.Empty;
            switch (type)
            {
                case Type.Of:
                    day = $"{AddOrdinalSuffix(dateTime.Value.Day)} of";
                    break;

                case Type.DayOf:
                    day = $"{AddOrdinalSuffix(dateTime.Value.Day)} day of";
                    break;

                default:
                    day = $"{AddOrdinalSuffix(dateTime.Value.Day)}";
                    break;
            }

            var month = dateTime.Value.ToString("MMMM");

            return $"{day} {month} {dateTime.Value.Year}";
        }

        public static string AddOrdinalSuffix(int day)
        {
            var last2Digits = Math.Abs(day % 100);
            var lastDigit = last2Digits % 10;

            return day + "thstndrd".Substring(last2Digits > 10 && last2Digits < 14 || lastDigit > 3 ? 0 : lastDigit * 2,
                2);
        }
    }
}