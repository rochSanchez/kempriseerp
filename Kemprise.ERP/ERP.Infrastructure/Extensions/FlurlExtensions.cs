﻿using Flurl;

namespace ERP.Infrastructure.Extensions
{
    public static class FlurlExtensions
    {
        public static Url AppendCallParameters(this Url url, params object[] parameters)
        {
            if (parameters == null) return url;
            parameters.ForEach(param => url.AppendPathSegment(param));
            return url;
        }
    }
}