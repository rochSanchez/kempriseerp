﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ERP.Infrastructure.Extensions
{
    public static class ObjectExtensions
    {
        public static void IfNotNullThen<T>(this T o, Action<T> action) where T : class
        {
            if ((object)o == null)
                return;
            action(o);
        }

        public static bool IsTypeOf<T>(this object o) => o.GetType() == typeof(T);

        public static T As<T>(this object o) where T : class => o as T;

        public static void AddTo<T>(this T item, IList<T> list) => list.Add(item);

        public static U Select<T, U>(this T item, Func<T, U> func) => func(item);

        public static object GetDefault(this Type type) => type.IsValueType ? Activator.CreateInstance(type) : (object)null;

        public static R IfNotNull<T, R>(this T o, Func<T, R> returnFunc, R elseVal) => ((object)o).IsNull() ? elseVal : returnFunc(o);

        public static R IfNotNull<T, R>(this T o, Func<T, R> returnFunc) => o.IfNotNull<T, R>(returnFunc, default(R));

        public static void IfIsOfType<T>(this object o, Action<T> action) where T : class
        {
            if (!(o is T obj))
                return;
            action(obj);
        }

        public static bool IsNull(this object obj) => obj == null;

        public static bool IsNotNull(this object obj) => obj != null;

        public static VALUE TryGetValue<KEY, VALUE>(this IDictionary<KEY, VALUE> dict, KEY key) => dict.ContainsKey(key) ? dict[key] : default(VALUE);

        public static IEnumerable<T> WrapInEnumerable<T>(this T obj)
        {
            if (((object)obj).IsNull())
                return (IEnumerable<T>)new T[0];
            return (IEnumerable<T>)new T[1] { obj };
        }

        public static bool NullSafeEquals(this object x, object y) => x.IsNull() && y.IsNull() || x.IsNotNull() && x.Equals(y) || y.Equals(x);

        public static R Swallow<R>(this Func<R> func, params Type[] exceptions)
        {
            try
            {
                return func();
            }
            catch (Exception ex)
            {
                if (((IEnumerable<Type>)exceptions).Any<Type>((Func<Type, bool>)(x => x.IsAssignableFrom(ex.GetType()))))
                    return default(R);
                throw;
            }
        }

        public static void SetProperty(this object o, string propertyName, object value)
        {
            o.GetType().GetProperty(propertyName);
            o.GetType().GetProperty(propertyName).SetValue(o, value, (object[])null);
        }

        public static object GetPropertyValue(this object o, string propertyName) => o.GetType().GetProperty(propertyName).GetValue(o, (object[])null);

        public static MemberInfo GetMemberInfo(this Expression expression)
        {
            LambdaExpression lambdaExpression = (LambdaExpression)expression;
            return (!(lambdaExpression.Body is UnaryExpression) ? (MemberExpression)lambdaExpression.Body : (MemberExpression)((UnaryExpression)lambdaExpression.Body).Operand).Member;
        }
    }
}