﻿using System;
using System.ComponentModel;

namespace ERP.Infrastructure.Extensions
{
    public static class ActionExtensions
    {
        public static void IfNotNullInvoke<T>(this Action<T> action, T parameter)
        {
            action?.Invoke(parameter);
        }

        public static void RunAsync(this Action action, Action uponCompletion)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, e) => action();
            worker.RunWorkerCompleted += (sender, e) => uponCompletion();

            worker.RunWorkerAsync();
        }

        public static void RunAsync(this Action action)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, e) => action();
            worker.RunWorkerAsync();
        }
    }
}