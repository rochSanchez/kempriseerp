﻿using System;
using System.Windows;

namespace ERP.Infrastructure.Extensions
{
    public static class ViewExtensions
    {
        public static object GetViewModel(this FrameworkElement view)
        {
            var userControl = view;
            return userControl == null ? null : view.DataContext;
        }

        public static T GetViewModel<T>(this FrameworkElement view) where T : class
        {
            var userControl = view;
            return userControl == null ? default : view.DataContext.As<T>();
        }

        public static void SetValueToProperty(this object o, string propertyName, object newValue)
        {
            var propertyInfo = o.GetType().GetProperty(propertyName);
            o.SetProperty(propertyName, propertyInfo.PropertyType == typeof(decimal)
                ? Convert.ToDecimal(newValue)
                : newValue);
        }
    }
}