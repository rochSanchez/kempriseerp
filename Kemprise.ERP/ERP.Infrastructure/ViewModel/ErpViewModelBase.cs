﻿using ERP.Infrastructure.Events.Errors;
using ERP.Infrastructure.Extensions;
using ERP.Infrastructure.MethodCalls;
using ERP.Infrastructure.Modules;
using ERP.Infrastructure.PropertyChangeNotification;
using ERP.Infrastructure.Services.APICommon;
using ERP.Infrastructure.Services.Lookup;
using ERP.Infrastructure.Services.Notifications;
using ERP.WebApi.DTO.Lookups;
using Flurl.Http;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace ERP.Infrastructure.ViewModel
{
    public class ErpViewModelBase : NotifyPropertyChangedBase, IErpViewModelBase
    {
        private readonly string _category;
        private readonly IDesktopNotificationService _desktopNotificationService;
        private readonly ErpModules _moduleName;

        protected ErpViewModelBase(IUnityContainer container, ErpModules moduleName, string category)
        {
            _moduleName = moduleName;
            _category = category;
            Container = container;

            _desktopNotificationService = Container.Resolve<IDesktopNotificationService>();
            EventAggregator = container.Resolve<IEventAggregator>();
            LookupService = container.Resolve<ILookupService>();

            Lookups = new Dictionary<string, IEnumerable<LookupEntity>>();
            Methods = new Dictionary<string, IErpMethodCall>();

            ConfigureMethodCalls(moduleName, category);
        }

        public string BusyContent
        {
            get { return GetValue(() => BusyContent); }
            set { SetValue(() => BusyContent, value); }
        }

        public IUnityContainer Container { get; }
        public DynamicNotifyPropertyChangedProxy Entity { get; set; }
        public IEventAggregator EventAggregator { get; }

        public bool IsBusy
        {
            get { return GetValue(() => IsBusy); }
            set { SetValue(() => IsBusy, value); }
        }

        public Dictionary<string, IEnumerable<LookupEntity>> Lookups
        {
            get { return GetValue(() => Lookups); }
            set { SetValue(() => Lookups, value); }
        }

        public ILookupService LookupService { get; set; }
        public Dictionary<string, IErpMethodCall> Methods { get; set; }

        public async Task<OperationResponse> GetRecordAsync(string methodName)
        {
            OperationResponse result = null;

            try
            {
                IsBusy = true;
                if (Methods.ContainsKey(methodName))
                {
                    var method = Methods[methodName].As<GetMethodCallBase>();
                    BusyContent = method.BusyContent;
                    await method.RunAsync(Entity).ContinueWith(task =>
                    {
                        if (task.Status == TaskStatus.RanToCompletion && task.Exception != null)
                        {
                            result = new OperationResponse(methodName, OperationResult.Failed);
                            throw new Exception(task.Exception.Message);
                        }

                        if (task.Status == TaskStatus.Faulted)
                        {
                            result = new OperationResponse(methodName, OperationResult.Failed,
                               task.Exception?.Message);
                            throw new Exception(task.Exception?.Message);
                        }

                        result = new OperationResponse(methodName, OperationResult.Success);
                    });
                    if (method.WhenDone != null)
                        await Task.Run(() => method.WhenDone()).ContinueWith(task =>
                        {
                            var responses = new List<OperationResponse>();
                            AddOperationResponse(task, responses, string.Concat(method.MethodName, "-whenDone"));
                            result = responses.FirstOrDefault();
                        });
                }
            }
            catch (Exception e)
            {
                result = new OperationResponse(methodName, OperationResult.Failed, e.Message);
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload(e.Message));
                return result;
            }
            finally
            {
                IsBusy = false;
            }

            return result;
        }

        public async Task<IEnumerable<OperationResponse>> RunStartupMethodsAsync(string parameter)
        {
            var result = new List<OperationResponse>();
            try
            {
                IsBusy = true;
                var startupMethods = Methods.Values.OfType<IErpGetMethodCall>()
                    .Where(getMethodCall => getMethodCall.RunAtStartup).ToList();
                RegisterLookups();
                SetDefaultParameter(parameter);
                Lookups = await GetLookupsAsync();
                foreach (var method in startupMethods)
                {
                    if (!Entity.GetDto<EntityBase>().IsNew)
                    {
                        await method.RunAsync(Entity).ContinueWith(task =>
                        {
                            AddOperationResponse(task, result, method.MethodName);
                        });
                    }

                    if (result.Any(response => response.OperationResult == OperationResult.Failed)) break;

                    if (method.WhenDone != null)
                        await Task.Run(() => method.WhenDone()).ContinueWith(task =>
                        {
                            AddOperationResponse(task, result, string.Concat(method.MethodName, "-whenDone"));
                        });
                }

                return result;
            }
            catch (Exception e)
            {
                result.Add(new OperationResponse("", OperationResult.Failed, e.Message));
                return result;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task<OperationResponse> SaveRecordAsync(string methodName)
        {
            OperationResponse result = null;
            try
            {
                IsBusy = true;
                if (Methods.ContainsKey(methodName))
                {
                    var method = Methods[methodName].As<ErpSaveMethodCallBase>();
                    BusyContent = method.BusyContent;
                    await method.SaveRecordAsync(Entity.GetDto<EntityBase>()).ContinueWith(async task =>
                    {
                        var errorMessage = "";

                        if (task.Exception != null)
                        {
                            if (task.Exception.GetBaseException() is FlurlHttpException flurlHttpException)
                                errorMessage = await flurlHttpException.GetResponseStringAsync();
                            else
                                errorMessage = task.Exception.Message;
                        }

                        result = task.Status == TaskStatus.RanToCompletion && task.Exception == null
                            ? new OperationResponse(methodName, OperationResult.Success)
                            : new OperationResponse(methodName, OperationResult.Failed, errorMessage);
                    });

                    if (result.OperationResult == OperationResult.Success)
                    {
                        if (method.ShowNotification)
                            DisplayNotification();
                    }
                    else
                    {
                        EventAggregator.GetEvent<ErrorEvent>()
                            .Publish(new ErrorEventPayload(result.ErrorMessage));
                    }
                }
            }
            catch (Exception e)
            {
                result = new OperationResponse(methodName, OperationResult.Failed, e.Message);
                EventAggregator.GetEvent<ErrorEvent>()
                    .Publish(new ErrorEventPayload(e.Message));
                return result;
            }
            finally
            {
                IsBusy = false;
            }

            return result;
        }

        protected static void AddOperationResponse(Task t, ICollection<OperationResponse> list, string methodName)
        {
            var methodResult = t.Status == TaskStatus.RanToCompletion
                ? OperationResult.Success
                : OperationResult.Failed;
            var errorMessage = t.Exception == null
                ? ""
                : t.Exception.InnerExceptions.Any()
                    ? t.Exception.InnerExceptions.First().Message
                    : t.Exception.Message;

            list.Add(new OperationResponse(methodName, methodResult, errorMessage));
        }

        protected void ConfigureMethodCalls(ErpModules moduleName, string category)
        {
            var methods = Container.ResolveAll<IErpMethodCall>()
                .Where(methodCall => methodCall.ModuleName == moduleName)
                .Where(call => call.Category == category);
            methods.ForEach(methodCall => Methods.Add(methodCall.MethodName, methodCall));
        }

        protected async Task<Dictionary<string, IEnumerable<LookupEntity>>> GetLookupsAsync()
        {
            return await LookupService.GetLookupsAsync();
        }

        protected async void RefreshLookup<T>(ApiControllers controller, string method, string lookup) where T : Lookups
        {
            IsBusy = true;
            var lookupData = await LookupService.RefreshLookupAsync<T>(controller, method);
            Lookups[lookup] = lookupData[lookup];
            IsBusy = false;
        }

        protected virtual void RefreshView()
        {
        }

        protected virtual void RegisterLookups()
        {
        }

        private void DisplayNotification()
        {
            _desktopNotificationService.NotifyUser(new SaveSuccessfulAlert(new NotificationInfo
            { ModuleName = _moduleName.ToString().ToUpper() }));
        }

        private async Task<Dictionary<string, IEnumerable<LookupEntity>>> RefreshLookups<T>(ApiControllers controller, string method) where T : Lookups
        {
            return await LookupService.RefreshLookupAsync<T>(controller, method);
        }

        private void SetDefaultParameter(string parameter)
        {
            Methods.Values.Where(x => x.UseDefaultParameter)
                .ForEach(x => x.MethodParameter = parameter);
        }
    }
}