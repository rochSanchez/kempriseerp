﻿using Prism.Ioc;
using Prism.Modularity;
using System.Reflection;

namespace ERP.Infrastructure.Modules
{
    public abstract class ErpModule : IModule
    {
        protected ErpModule()
        {
            Initialize();
        }

        public virtual void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }

        public virtual void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void Initialize()
        {
            MergeResourceDictionaries();
        }

        private void MergeResourceDictionaries()
        {
            ResourceDictionaryLoader.Create()
                .LoadResourceDictionaries(GetModuleAssembly())
                .LoadResourceDictionaries(GetBusinessAssembly());
        }

        protected abstract Assembly GetBusinessAssembly();

        protected abstract Assembly GetModuleAssembly();
    }
}