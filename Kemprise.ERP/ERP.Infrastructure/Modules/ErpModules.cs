﻿namespace ERP.Infrastructure.Modules
{
    public enum ErpModules
    {
        Home,
        Inquiry,
        Sales,
        Management
    }
}