﻿namespace ERP.Infrastructure.Modules
{
    public interface IQuickAccess
    {
        bool IsSelected { get; set; }
    }
}