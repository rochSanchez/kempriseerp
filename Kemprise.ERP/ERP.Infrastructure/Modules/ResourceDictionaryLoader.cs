﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ERP.Infrastructure.Modules
{
    public class ResourceDictionaryLoader
    {
        /// <summary>
        ///     Creates an instance of a Resource Dictionary Loader.
        /// </summary>
        /// <returns></returns>
        public static ResourceDictionaryLoader Create()
        {
            return new ResourceDictionaryLoader();
        }

        /// <summary>
        ///     Loads the resource dictionaries from the assembly.
        /// </summary>
        public ResourceDictionaryLoader LoadResourceDictionaries(Assembly assembly)
        {
            var assemblyName = assembly.GetName().Name;
            var stream = assembly.GetManifestResourceStream(assemblyName + ".g.resources");

            if (stream == null)
                return this;

            using (var reader = new ResourceReader(stream))
            {
                foreach (DictionaryEntry entry in reader)
                {
                    var key = (string)entry.Key;

                    if (key.Contains(".baml"))
                    {
                        AddResourceDictionary(assemblyName, key);
                    }
                    else
                    {
                        var imageSuffixes = new[] { ".png", ".jpg", ".gif" };

                        foreach (var suffix in imageSuffixes)
                            if (key.Contains(suffix))
                                AddImage(entry, suffix);
                    }
                }
            }

            return this;
        }

        private void AddImage(DictionaryEntry entry, string imageSuffix)
        {
            var key = entry.Key.ToString();

            var startIndex = key.LastIndexOf('/') + 1;
            var endIndex = key.LastIndexOf(imageSuffix, StringComparison.Ordinal);
            var length = endIndex - startIndex;
            var imageName = entry.Key.ToString().Substring(startIndex, length);

            var chars = imageName.ToCharArray();
            chars[0] = char.ToUpper(chars[0]);

            var newKey = new string(chars) + "Image";

            var i = new BitmapImage();
            i.BeginInit();
            i.StreamSource = entry.Value as Stream;
            i.CacheOption = BitmapCacheOption.OnLoad;
            i.CreateOptions = BitmapCreateOptions.None;
            i.EndInit();

            try
            {
                Application.Current.Resources.Add(newKey, i);
            }
            catch
            {
            }
        }

        private void AddResourceDictionary(string assemblyName, string key)
        {
            key = key.Replace(".baml", ".xaml");

            var uriString = string.Format("pack://application:,,,/{0};Component/{1}", assemblyName, key);

            try
            {
                if (!uriString.ToUpper().Contains("VIEW.XAML")
                    && !uriString.ToUpper().Contains("APP.XAML"))
                {
                    var dictionary = new ResourceDictionary { Source = new Uri(uriString) };
                    Application.Current.Resources.MergedDictionaries.Add(dictionary);
                }
            }
            catch (Exception)
            {
            }
        }
    }
}