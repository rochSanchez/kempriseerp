﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;
using Unity;

namespace ERP.Infrastructure.Modules.Navigation
{
    public abstract class ChildNavigationItemBase : IChildNavigationItem

    {
        public bool IsVisible { get; set; }
        private readonly IUnityContainer _container;

        protected ChildNavigationItemBase(IUnityContainer container)
        {
            _container = container;
            IsVisible = true;
        }

        public abstract string Name { get; }
        public virtual object View => null;
        public abstract Uri Icon { get; }
        public abstract IParentNavigationItem Parent { get; }
        public abstract ErpModules Module { get; }

        public virtual async Task<IEnumerable<EntityListItem>> GetRecordsAsync()
        {
            return await Task.Run(() => new List<EntityListItem>());
        }
    }
}