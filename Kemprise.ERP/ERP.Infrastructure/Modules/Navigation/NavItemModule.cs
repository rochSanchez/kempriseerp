﻿namespace ERP.Infrastructure.Modules.Navigation
{
    public enum NavItemModule
    {
        Management, Sales
    }
}