﻿using System.Collections.Generic;

namespace ERP.Infrastructure.Modules.Navigation
{
    public interface IParentNavigationItem : INavigationItem
    {
        IEnumerable<INavigationItem> Children { get; set; }
    }
}