﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.DTO;

namespace ERP.Infrastructure.Modules.Navigation
{
    public interface INavigationItem
    {
        string Name { get; }
        object View { get; }
        Uri Icon { get; }
        ErpModules Module { get; }
        Task<IEnumerable<EntityListItem>> GetRecordsAsync();
    }
}