﻿namespace ERP.Infrastructure.Modules.Navigation
{
    public interface IChildNavigationItem : INavigationItem
    {
        public IParentNavigationItem Parent { get; }
    }
}