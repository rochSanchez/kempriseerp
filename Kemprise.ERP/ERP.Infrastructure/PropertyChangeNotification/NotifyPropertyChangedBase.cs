﻿using ERP.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public abstract class NotifyPropertyChangedBase : PropertyChangedBase
    {
        private readonly Dictionary<string, object> _backingValues = new Dictionary<string, object>();

        protected NotifyPropertyChangedBase()
        {
            PropertyValuesRepository = new Dictionary<string, List<object>>();
        }

        public virtual bool HasChanges { get; set; }
        public Dictionary<string, List<object>> PropertyValuesRepository { get; set; }

        protected void ClearValue(string propertyName)
        {
            _backingValues[propertyName] = null;
            NotifyOfPropertyChange(propertyName);
        }

        protected TProperty GetValue<TProperty>(Expression<Func<TProperty>> property)
        {
            var propertyName = property.GetMemberInfo().Name;

            return (TProperty)GetCurrentValue<TProperty>(propertyName);
        }

        protected void InitializeProperty<TProperty>(Expression<Func<TProperty>> property, object value)
        {
            var newValue = (TProperty)value;

            var propertyName = GetPropertyName(property);

            var currentValue = (TProperty)GetCurrentValue<TProperty>(propertyName);

            if (AreEqual(currentValue, newValue))
                return;

            _backingValues[propertyName] = value;
        }

        protected void MarkAsChanged()
        {
            HasChanges = true;
        }

        protected virtual void RaisePropertyChanged<TProperty>(Expression<Func<TProperty>> property)
        {
            NotifyOfPropertyChange(property.GetMemberInfo().Name);
        }

        protected void SetValue<TProperty>(Expression<Func<TProperty>> property, object value,
            Action onChange = null)
        {
            var newValue = (TProperty)value;

            var propertyName = property.GetMemberInfo().Name;

            var currentValue = (TProperty)GetCurrentValue<TProperty>(propertyName);
            PropertyValuesRepository[propertyName].Add(newValue);
            if (AreEqual(currentValue, newValue))
                return;

            MarkAsChanged();

            _backingValues[propertyName] = value;

            NotifyOfPropertyChange(propertyName);
            if (onChange.IsNotNull())
                onChange();
        }

        protected void UnMarkAsChanged()
        {
            HasChanges = false;
        }

        private static bool AreEqual<TProperty>(TProperty currentValue, TProperty newValue)
        {
            if (currentValue.IsNull() && newValue.IsNull())
                return true;

            if (currentValue.IsNotNull())
                return currentValue.Equals(newValue);

            return newValue.Equals(currentValue);
        }

        private object GetCurrentValue<TProperty>(string propertyName)
        {
            if (!_backingValues.ContainsKey(propertyName))
            {
                _backingValues.Add(propertyName, default(TProperty));
                PropertyValuesRepository.Add(propertyName, new List<object>());
            }

            return _backingValues[propertyName];
        }

        private string GetPropertyName<TProperty>(Expression<Func<TProperty>> property)
        {
            return property.GetMemberInfo().Name;
        }
    }
}