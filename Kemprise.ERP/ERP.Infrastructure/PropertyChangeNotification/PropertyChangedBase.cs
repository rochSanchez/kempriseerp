﻿using ERP.Infrastructure.Extensions;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public class PropertyChangedBase : INotifyPropertyChangedEx
    {
        public PropertyChangedBase()
        {
            IsNotifying = true;
        }

        [field: NonSerialized] public virtual event PropertyChangedEventHandler PropertyChanged = delegate { };

        public virtual bool IsNotifying { get; set; }

        public virtual void NotifyOfPropertyChange(string propertyName)
        {
            if (IsNotifying)
                Execute.OnUiThread(() => RaisePropertyChangedEventCore(propertyName));
        }

        public virtual void Refresh()
        {
            NotifyOfPropertyChange(string.Empty);
        }

        public virtual void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property)
        {
            NotifyOfPropertyChange(property.GetMemberInfo().Name);
        }

        public virtual void RaisePropertyChangedEventImmediately(string propertyName)
        {
            if (IsNotifying)
                RaisePropertyChangedEventCore(propertyName);
        }

        private void RaisePropertyChangedEventCore(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}