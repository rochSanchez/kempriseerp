﻿using System.Reflection;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public class ChangedProperty
    {
        public ChangedProperty(string propertyName, dynamic initialValue, dynamic currentValue,
            PropertyInfo propertyInfo)
        {
            PropertyName = propertyName;
            InitialValue = initialValue;
            CurrentValue = currentValue;
            PropertyInfo = propertyInfo;
        }

        public dynamic CurrentValue { get; }
        public dynamic InitialValue { get; }
        public PropertyInfo PropertyInfo { get; set; }
        public string PropertyName { get; }
    }
}