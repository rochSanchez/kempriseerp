﻿using ERP.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public class DynamicNotifyPropertyChangedProxy : DynamicObject, INotifyPropertyChanged
    {
        private readonly object _dto;
        private readonly Dictionary<string, RowPropertyValue> _members = new Dictionary<string, RowPropertyValue>();

        public DynamicNotifyPropertyChangedProxy(object dto)
        {
            _dto = dto;
            RegisterStaticallyCreatedProperties(dto);
        }

        public event PropertyChangedEventHandler PropertyChanged = (sender, args) => { };

        public void DiscardChanges()
        {
            var result = _members.Values
                .Where(propertyValue => propertyValue.Changed);
            result.ForEach(rowPropertyValue =>
            {
                this.SetProperty(rowPropertyValue.PropertyName, rowPropertyValue.InitialValue);
            });
        }

        public T GetDto<T>() where T : class
        {
            return _dto as T;
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _members.Keys;
        }

        public IEnumerable<PropertyInfo> GetPropertiesInfos()
        {
            return _members.Values.Select(x => x.PropertyInfo);
        }

        public dynamic GetProperty(string propertyName)
        {
            if (!_members.ContainsKey(propertyName))
                throw new InvalidOperationException($"Unregistered member: {propertyName}");
            return _members[propertyName].Get();
        }

        public object GetValue(string propertyName)
        {
            return _members[propertyName].CurrentValue;
        }

        public virtual void SetProperty(string propertyName, dynamic value)
        {
            var valueChanged = false;

            if (!_members.ContainsKey(propertyName))
                throw new InvalidOperationException($"Unregistered member: {propertyName}");
            if (_members[propertyName].Get() != value)
            {
                _members[propertyName].Set(value);
                valueChanged = true;
            }

            if (valueChanged) RaisePropertyChanged(propertyName);
        }

        public override string ToString()
        {
            return _dto.ToString();
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (_members.ContainsKey(binder.Name))
            {
                var propertyValue = _members[binder.Name];
                result = propertyValue.Get();
                return true;
            }

            result = null;
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            SetProperty(binder.Name, value);
            return true;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Register(string propertyName, Func<string, object> valueGetter,
            Action<string, object> valueSetter = null,
            PropertyInfo prop = null)
        {
            var fullPropertyName = propertyName;

            if (_members.ContainsKey(fullPropertyName))
                return;
            dynamic initialValue = valueGetter.Invoke(propertyName);
            var propertyValue = new RowPropertyValue(propertyName, valueGetter, valueSetter, initialValue, prop);
            _members.Add(fullPropertyName, propertyValue);
        }

        private void RegisterStaticallyCreatedProperties(object entity)
        {
            var entityType = entity.GetType();

            entityType
                .GetProperties(BindingFlags.Public | BindingFlags.Instance).ToList()
                .ForEach(prop =>
                {
                    if (prop != null && prop.GetSetMethod() != null)
                    {
                        object ValueGetter(string p)
                        {
                            return prop.GetValue(entity, null);
                        }

                        void ValueSetter(string p, object newValue)
                        {
                            prop.SetValue(entity, newValue, null);
                        }

                        Register(prop.Name, ValueGetter, ValueSetter, prop);
                    }
                    else
                    {
                        object ValueGetter(string p)
                        {
                            return prop.GetValue(entity, null);
                        }

                        Register(prop?.Name, ValueGetter);
                    }
                });
        }
    }
}