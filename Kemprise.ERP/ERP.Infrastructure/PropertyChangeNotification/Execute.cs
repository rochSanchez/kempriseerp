﻿using System;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public static class Execute
    {
        public static Action<Action> Executor = action => action();

        public static void OnUiThread(Action action)
        {
            Executor(action);
        }
    }
}