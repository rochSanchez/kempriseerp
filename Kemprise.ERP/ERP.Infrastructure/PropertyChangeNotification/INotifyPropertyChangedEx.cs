﻿using System.ComponentModel;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public interface INotifyPropertyChangedEx : INotifyPropertyChanged
    {
        bool IsNotifying { get; set; }

        void NotifyOfPropertyChange(string propertyName);

        void Refresh();
    }
}