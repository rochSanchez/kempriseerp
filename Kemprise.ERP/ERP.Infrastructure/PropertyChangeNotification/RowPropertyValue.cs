﻿using System;
using System.Reflection;

namespace ERP.Infrastructure.PropertyChangeNotification
{
    public class RowPropertyValue
    {
        private readonly Func<string, object> _getter;
        private readonly Action<string, object> _setter;

        public RowPropertyValue(string propertyName, Func<string, object> getter, Action<string, object> setter = null,
            dynamic initialValue = null, PropertyInfo property = null)
        {
            PropertyName = propertyName;
            InitialValue = initialValue;
            CurrentValue = initialValue;
            PropertyInfo = property;
            _getter = getter;
            _setter = setter;
        }

        public bool Changed => InitialValue != CurrentValue;

        public dynamic CurrentValue { get; set; }
        public dynamic InitialValue { get; }
        public PropertyInfo PropertyInfo { get; set; }
        public string PropertyName { get; }

        public dynamic Get()
        {
            return _getter(PropertyName);
        }

        public void Set(dynamic value)
        {
            _setter(PropertyName, value);
            CurrentValue = value;
        }
    }
}